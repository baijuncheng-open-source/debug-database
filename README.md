

# Debug Database

## Debug Database is a powerful library for debugging databases and preferences in HarmonyOS applications

### Debug Database allows you to view databases and preferences directly in your browser in a very simple way

### What can Debug Database do?

* See all the databases.
* See all the data in the preferences used in your application.
* Run any sql query on the given database to update and delete your data.
* Directly edit the database values.
* Directly edit the preferences.
* Directly add a row in the database.
* Directly add a key-value in the preferences.
* Delete database rows and preferences.
* Search in your data.
* Sort data.
* Download database.
* Debug Room inMemory database.

### All these features work without rooting your device -> No need of rooted device

### Using Debug Database Library in your application

#### Solution 1: local source code integration, users can customize base on the source code

1.Copy debug_db/debug_db_base/debug_db_encrypt three folders to the project directory;

2.Modify project settings.gradle, add dependencies on three modules as follows:

```
include ':debug_db', ':debug_db_base', ':debug_db_encrypt'
```

3.Introduce the dependency of imported module in the project. Take the entry module as an example,
you need to modify the dependency of imported module in the entry module build.gradle file to add dependencies:


```
dependencies {
    entryImplementation project(':entry')
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    debugImplementation project(path: ':debug_db_encrypt')
    debugImplementation project(path: ':debug_db_base')
    debugImplementation project(path: ':debug_db')
}
```


#### Solution 2: local har package integration

1.Compile the project and copy the har package generated in the build directory of folder debug_db/debug_db_encrypt to the project lib folder

2.Add the following code in gradle of entry

```
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

#### Solution 3: remote maven repo integration

1.add mavenCentral dependency at repositories in your project build.gradle

```
allprojects {
    repositories {
        mavenCentral()
    }
}
```

2.add library dependency at your module build.gradle

```xml
implementation 'com.gitee.baijuncheng-open-source:debug_db:1.0.0'
implementation 'com.gitee.baijuncheng-open-source:debug_db_base:1.0.0'
implementation 'com.gitee.baijuncheng-open-source:debug_db_encrypt:1.0.0'
```

Use `debugImplementation` so that it will only compile in your debug build and not in your release build.

That’s all, just start the application, you will see in the logcat an entry like follows :

* D/DebugDB: Open http://XXX.XXX.X.XXX:8080 in your browser

* You can also always get the debug address url from your code by calling the method `DebugDB.getAddressLog();`

Now open the provided link in your browser.

Important:

* Your HarmonyOS device should be connected to the same Network (Wifi or LAN).
* If you are using it over usb to connect with your pc, run `hdc fport tcp:8080 tcp:8080`, and open http://localhost:8080 at your pc browser

Note: If you want use different port other than 8080.
            In the app build.gradle file under buildTypes do the following change

```groovy
debug {
    resValue("string", "PORT_NUMBER", "8081")
}
```

You will see something like this :

### Seeing values

![查看](https://images.gitee.com/uploads/images/2021/0309/102406_f4d62e1f_8230582.png "debugdb.png")

### Editing values

![编辑](https://images.gitee.com/uploads/images/2021/0309/102416_25fa542e_8230582.png "debugdb_edit.png")


### Working with remote device

* HarmonyOS remote device: Run the command in the terminal - `hdc fport tcp:8080 tcp:8080` and open http://localhost:8080

### Getting address with toast dialog, in case you missed the address log in Logcat

As this library is auto-initialize, if you want to get the address log, add the following method and call (we have to do like this to avoid build error in release build as this library will not be included in the release build) using reflection.

```java
public static void showDebugDBAddressLogToast(Context context) {
    if (BuildConfig.DEBUG) {
        try {
            Class<?> debugDB = Class.forName("com.amitshekhar.DebugDB");
            Method getAddressLog = debugDB.getMethod("getAddressLog");
            Object value = getAddressLog.invoke(null);
            new ToastDialog(context).setText((String) value).setDuration(4000).show();
        } catch (Exception ignore) {

        }
    }
}
```

### Adding custom database files

As this library is auto-initialize, if you want to debug custom database files, add the following method and call

```java
public static void setCustomDatabaseFiles(Context context) {
    if (BuildConfig.DEBUG) {
        try {
            Class<?> debugDB = Class.forName("com.amitshekhar.DebugDB");
            Class[] argTypes = new Class[]{HashMap.class};
            Method setCustomDatabaseFiles = debugDB.getMethod("setCustomDatabaseFiles", argTypes);
            HashMap<String, Pair<File, String>> customDatabaseFiles = new HashMap<>();
            // set your custom database files
            customDatabaseFiles.put(ExtTestDBHelper.DATABASE_NAME,
                    new Pair<>(new File(context.getFilesDir() + "/" + ExtTestDBHelper.DIR_NAME +
                            "/" + ExtTestDBHelper.DATABASE_NAME), ""));
            setCustomDatabaseFiles.invoke(null, customDatabaseFiles);
        } catch (Exception ignore) {

        }
    }
}
```

### Adding InMemory Room databases

As this library is auto-initialize, if you want to debug inMemory Room databases, add the following method and call

```java
public static void setInMemoryRoomDatabases(RdbStore... database) {
    if (BuildConfig.DEBUG) {
        try {
            Class<?> debugDB = Class.forName("com.amitshekhar.DebugDB");
            Class[] argTypes = new Class[]{HashMap.class};
            HashMap<String, RdbStore> inMemoryDatabases = new HashMap<>();
            // set your inMemory databases
            inMemoryDatabases.put("InMemoryOne.db", database[0]);
            Method setRoomInMemoryDatabase = debugDB.getMethod("setInMemoryRoomDatabases", argTypes);
            setRoomInMemoryDatabase.invoke(null, inMemoryDatabases);
        } catch (Exception ignore) {

        }
    }
}
```

### TODO

* And of course many more features and bug fixes.

### License

```
   Copyright (C) 2019 Amit Shekhar
   Copyright (C) 2011 Android Open Source Project

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```