package com.amitshekhar.sqlite;

import ohos.agp.utils.TextTool;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.when;
@PrepareForTest(value = {InMemoryDebugSQLiteDB.class, TextTool.class,RdbStore.class})
@RunWith(PowerMockRunner.class)
public class InMemoryDebugSQLiteDBTest {
    InMemoryDebugSQLiteDB inMemoryDebugSQLiteDB;
    RdbStore database;

    @Before
    public void setUp() throws Exception {
        mockStatic(TextTool.class);
        database = mock(RdbStore.class);
        inMemoryDebugSQLiteDB = new InMemoryDebugSQLiteDB(database);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void beginTransaction() {
        inMemoryDebugSQLiteDB.beginTransaction();
        verify(database,times(1)).beginTransaction();
    }

    @Test
    public void endTransaction() {
        inMemoryDebugSQLiteDB.endTransaction();
        verify(database,times(1)).endTransaction();
    }

    @Test
    public void getRealDb() {
        RdbStore realDb = inMemoryDebugSQLiteDB.getRealDb();
        assertNotNull(realDb);
    }

    @Test
    public void delete() throws Exception {
        RawRdbPredicates rdbPredicates = mock(RawRdbPredicates.class);
        whenNew(RawRdbPredicates.class).withArguments("users").thenReturn(rdbPredicates);
        doNothing().when(rdbPredicates).setWhereClause(anyString());
        doNothing().when(rdbPredicates).setWhereArgs(anyList());
        int delete = inMemoryDebugSQLiteDB.delete("users", "11", new String[]{"11", "22"});
        assertEquals(delete,0);
    }

    @Test
    public void isOpen() {
        boolean isOpen = inMemoryDebugSQLiteDB.isOpen();
        assertEquals(isOpen,false);
    }

    @Test
    public void close() {
        inMemoryDebugSQLiteDB.close();
    }

    @Test
    public void rawQuery() {
        ResultSet resultSet = inMemoryDebugSQLiteDB.rawQuery("SELECT type FROM sqlite_master WHERE name=?", new String[]{"11", "22"});
        assertNull(resultSet);

    }

    @Test
    public void execSQL() {
        inMemoryDebugSQLiteDB.execSQL("select * from users");
        verify(database,times(1)).executeSql("select * from users");
    }

    @Test
    public void insert() throws Exception {
        ValuesBucket values = mock(ValuesBucket.class);
        Set<String> columnSet = new HashSet<>();
        when(values.getColumnSet()).thenReturn(columnSet);
        long insert = inMemoryDebugSQLiteDB.insert("users", "", values);
        assertEquals(insert,0);
    }

    @Test
    public void update() throws Exception {
        ValuesBucket values = mock(ValuesBucket.class);
        RawRdbPredicates rdbPredicates = mock(RawRdbPredicates.class);
        whenNew(RawRdbPredicates.class).withArguments("users").thenReturn(rdbPredicates);
        doNothing().when(rdbPredicates).setWhereClause(anyString());
        doNothing().when(rdbPredicates).setWhereArgs(anyList());
        int update = inMemoryDebugSQLiteDB.update("users", values, "", new String[]{"11", "22"});
        assertEquals(update,0);
    }

    @Test
    public void getVersion() {
        int version = inMemoryDebugSQLiteDB.getVersion();
        assertEquals(version,0);
    }

}