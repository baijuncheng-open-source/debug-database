package com.amitshekhar.server;

import com.amitshekhar.model.RowDataRequest;
import com.amitshekhar.sqlite.DBFactory;
import com.amitshekhar.utils.DatabaseFileProvider;
import ohos.data.DatabaseHelper;
import com.amitshekhar.utils.PrefHelper;
import com.amitshekhar.utils.Utils;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.data.rdb.RdbStore;
import ohos.global.resource.ResourceManager;
import ohos.utils.Pair;
import ohos.utils.net.Uri;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;


import java.io.*;
import java.net.*;
import java.util.*;
import ohos.data.preferences.Preferences;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.*;
@PrepareForTest({ZSONArray.class,RequestHandler.class, URLDecoder.class,TextTool.class,
        Uri.class,Utils.class, DatabaseFileProvider.class,
        ZSONObject.class, DatabaseHelper.class, PrefHelper.class})
@RunWith(PowerMockRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RequestHandlerTest {
    Context mContext;
    ResourceManager mResources;
    DBFactory dbFactory;
    RequestHandler requestHandler;
    int timeOut = 1000;

    @Before
    public void setUp() throws Exception {
        mContext = mock(Context.class);
        mResources = mock(ResourceManager.class);
        mockStatic(TextTool.class);
        mockStatic(ZSONObject.class);
        mockStatic(Utils.class);
        when(mContext.getApplicationContext()).thenReturn(mContext);
        when(mContext.getResourceManager()).thenReturn(mResources);
        requestHandler = new RequestHandler(mContext, dbFactory);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    //127.0.0.1:81/getDbList
    public void handleWhenGetDbList() throws Exception {
        execute("getDbList",91,"query=select%20*%20from%20a");
        Socket socket = new ServerSocket(91).accept();
        File databaseDir = new File("../../");
        System.out.println("path:"+databaseDir.getAbsolutePath());
        when(mContext.getDatabaseDir()).thenReturn(databaseDir);

        Pair pair = mock(Pair.class);
        Whitebox.setInternalState(pair,"s","txt.db");
        whenNew(Pair.class).withAnyArguments().thenReturn(pair);

        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withAnyArguments().thenReturn(databaseHelper);
        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences(anyString())).thenReturn(preferences);
        when(preferences.getString(anyString(),anyString())).thenReturn("aaaaa");
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");

        requestHandler.handle(socket);
        verifyStatic(ZSONObject.class,times(1));
        ZSONObject.toZSONString(any());
    }

    @Test
    //http://127.0.0.1/query?query=select%20*%20from%20a
    public void handleWhenQuery() throws Exception {
        execute("query",90,"query=select%20*%20from%20a");
        Socket socket = new ServerSocket(90).accept();
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");
        requestHandler.handle(socket);
        verifyStatic(ZSONObject.class,times(1));
        ZSONObject.toZSONString(any());
    }

    @Test
    public void getAllDataFromTheTableResponse() throws Exception {
        field(RequestHandler.class, "isDbOpened").set(requestHandler, true);
        execute("getAllDataFromTheTable",81,"tableName=users");
        Socket socket = new ServerSocket(81).accept();
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");
        requestHandler.handle(socket);
        verifyStatic(ZSONObject.class,times(1));
        ZSONObject.toZSONString(any());
    }

    @Test
    public void handleGetTableList() throws Exception {
        execute("getTableList",82,"database=APP_SHARED_PREFERENCES");
        Socket socket = new ServerSocket(82).accept();
        File databaseDir = new File("../../");
        when(mContext.getPreferencesDir()).thenReturn(databaseDir);
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");
        requestHandler.handle(socket);
        verifyStatic(ZSONObject.class,times(1));
        ZSONObject.toZSONString(any());
    }

    @Test
    public void handleAddTableData() throws Exception {
        execute("addTableData",83,"");
        Socket socket = new ServerSocket(83).accept();
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");
        requestHandler.handle(socket);
        verifyStatic(ZSONObject.class,times(1));
        ZSONObject.toZSONString(any());
    }

    @Test
    public void handleUpdateTableData() throws Exception {
        execute("updateTableData",84,null);
        Socket socket = new ServerSocket(84).accept();
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");
        requestHandler.handle(socket);
        verifyStatic(ZSONObject.class,times(1));
        ZSONObject.toZSONString(any());
    }

    @Test
    public void handleDeleteTableData() throws Exception {
        execute("deleteTableData",85,null);
        mockStatic(URLDecoder.class);
        mockStatic(Uri.class);
        mockStatic(ZSONArray.class);
        mock(RowDataRequest.class);
        List<RowDataRequest> rowDataRequest = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            rowDataRequest.add(mock(RowDataRequest.class));
        }
        Uri uri=mock(Uri.class);
        Socket socket = new ServerSocket(85).accept();
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");
        when(URLDecoder.decode(anyString(),anyString())).thenReturn("bbbb");
        when(Uri.parse(anyString())).thenReturn(uri);
        when(uri.getFirstQueryParamByKey(anyString())).thenReturn("bbbb");
        when(ZSONArray.stringToClassList(anyString(),any())).thenReturn(Collections.singletonList(rowDataRequest));
        field(RequestHandler.class, "mSelectedDatabase").set(requestHandler, "APP_SHARED_PREFERENCES");
        requestHandler.handle(socket);
        verifyStatic(ZSONObject.class,times(1));
        ZSONObject.toZSONString(any());
    }

    @Test
    public void handleDeleteDb() throws Exception {
        execute("deleteDb",87,null);
        Socket socket = new ServerSocket(87).accept();
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");
        requestHandler.handle(socket);
        verifyStatic(ZSONObject.class,times(1));
        ZSONObject.toZSONString(any());
    }

    @Test
    public void handleDownloadDb() throws Exception {
        execute("downloadDb",88,null);
        Socket socket = new ServerSocket(88).accept();
        when(Utils.getDatabase(null,null)).thenReturn("aaa".getBytes());
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");
        requestHandler.handle(socket);
        verifyStatic(Utils.class,times(1));
        Utils.getDatabase(any(),any());
    }

    @Test
    public void handleWhenIsNull() throws Exception {
        execute("",89,null);
        Socket socket = new ServerSocket(89).accept();
        when(Utils.loadContent(anyString(), any())).thenReturn("aaa".getBytes());
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");
        requestHandler.handle(socket);
        verifyStatic(Utils.class,times(1));
        Utils.loadContent(anyString(), any());
    }

    @Test
    public void handleWhenReturnNull() throws Exception {
        execute("",94,null);
        Socket socket = new ServerSocket(94).accept();
        when(Utils.loadContent(anyString(), any())).thenReturn(null);
        when(ZSONObject.toZSONString(any())).thenReturn("bbbb");
        requestHandler.handle(socket);
        verifyStatic(Utils.class,times(1));
        Utils.loadContent(anyString(), any());
    }

    public void execute(String sendUrl,int port ,String params){
        new Thread(){
            @Override
            public void run() {
                try {
                    sleep(timeOut);
                    String url = "http://127.0.0.1:"+port+"/";
                    String sentUrl = url+sendUrl;
                    sendGet(sentUrl,params);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    try {
                        throw new IOException();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        }.start();
    }


    public static String sendGet(String url, String param)throws  IOException{
        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = url + "?" + param;
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }

    @Test
    public void setCustomDatabaseFiles() {
        HashMap<String, Pair<File, String>> customDatabaseFiles = new HashMap<>();
        requestHandler.setCustomDatabaseFiles(customDatabaseFiles);
    }

    @Test
    public void setInMemoryRoomDatabases() {
        HashMap<String, RdbStore> databases = new HashMap<>();
        requestHandler.setInMemoryRoomDatabases(databases);
    }
}