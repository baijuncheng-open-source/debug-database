package com.amitshekhar.server;

import com.amitshekhar.sqlite.DBFactory;
import com.amitshekhar.utils.Utils;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.data.rdb.RdbStore;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.ResourceManager;
import ohos.utils.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.support.membermodification.MemberMatcher.field;

@PrepareForTest({Logger.class, TextTool.class, Utils.class,RequestHandler.class})
@RunWith(PowerMockRunner.class)
public class ClientServerTest {
    ClientServer clientServer;

    @Before
    public void setUp() throws Exception {
        mockStatic(TextTool.class);
        Context context = Mockito.mock(Context.class);
        DBFactory dbFactory = Mockito.mock(DBFactory.class);
        clientServer = new ClientServer(context, 900, dbFactory);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void start() throws IllegalAccessException {
        clientServer.start();
        boolean mIsRunning = (boolean) field(ClientServer.class, "mIsRunning").get(clientServer);
        assertEquals(mIsRunning,true);
    }

    @Test
    public void stop() throws IOException, IllegalAccessException {
        field(ClientServer.class, "mServerSocket").set(clientServer, new ServerSocket());
        clientServer.stop();
        boolean mIsRunning = (boolean) field(ClientServer.class, "mIsRunning").get(clientServer);
        assertEquals(mIsRunning,false);
    }

    @Test
    public void run() throws Exception {
        RequestHandler handler= Whitebox.getInternalState(clientServer,"mRequestHandler");
        ResourceManager mResources=mock(ResourceManager.class);
        when(mResources.getRawFileEntry(anyString())).thenReturn(mock(RawFileEntry.class));
        Whitebox.setInternalState(handler,"mResources",mResources);
        Whitebox.setInternalState(clientServer, "mIsRunning", true);
        clientServer.run();
        boolean mIsRunning = (boolean) field(ClientServer.class, "mIsRunning").get(clientServer);
        assertEquals(mIsRunning,true);
    }

    @Test
    public void setCustomDatabaseFiles() {
        RequestHandler requestHandler = mock(RequestHandler.class);
        HashMap<String, Pair<File, String>> customDatabaseFiles = new HashMap<>();
        doNothing().when(requestHandler).setCustomDatabaseFiles(customDatabaseFiles);
        clientServer.setCustomDatabaseFiles(customDatabaseFiles);
        verify(requestHandler,times(0)).setCustomDatabaseFiles(customDatabaseFiles);

    }

    @Test
    public void setInMemoryRoomDatabases() {
        RequestHandler requestHandler = mock(RequestHandler.class);
        HashMap<String, RdbStore> databases = new HashMap<>();
        doNothing().when(requestHandler).setInMemoryRoomDatabases(databases);
        clientServer.setInMemoryRoomDatabases(databases);
        verify(requestHandler,times(0)).setInMemoryRoomDatabases(databases);
    }

    @Test
    public void isRunning() {
        boolean mIsRunning = clientServer.isRunning();
        assertEquals(mIsRunning,false);
    }
}