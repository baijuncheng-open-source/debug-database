package com.amitshekhar;

import com.amitshekhar.server.ClientServer;
import com.amitshekhar.sqlite.DBFactory;
import com.amitshekhar.utils.NetworkUtils;
import ohos.app.Context;
import ohos.eventhandler.InnerEvent;
import ohos.media.image.ImageSource;
import ohos.wifi.WifiDevice;
import ohos.wifi.WifiLinkedInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.support.membermodification.MemberMatcher.field;

@PrepareForTest({DBFactory.class, InnerEvent.class, ImageSource.class,WifiDevice.class, Optional.class, NetworkUtils.class,WifiLinkedInfo.class})
@RunWith(PowerMockRunner.class)
public class DebugDBTest {
    DebugDB debugDB;
    Context context;
    DBFactory dbFactory;
    ClientServer clientServer;
    @Before
    public void setUp() throws Exception {
        debugDB = mock(DebugDB.class);
        context = mock(Context.class);
        dbFactory = mock(DBFactory.class);
        clientServer = mock(ClientServer.class);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void initialize() {
        mockStatic(WifiDevice.class);
        WifiDevice wifiDevice = PowerMockito.mock(WifiDevice.class);
        PowerMockito.when(WifiDevice.getInstance(context)).thenReturn(wifiDevice);
        Optional<WifiLinkedInfo> ipInfo = PowerMockito.mock(Optional.class);
        PowerMockito.when(wifiDevice.getLinkedInfo()).thenReturn(ipInfo);
        PowerMockito.when(ipInfo.get()).thenReturn(PowerMockito.mock(WifiLinkedInfo.class));
        debugDB.initialize(context,dbFactory);
        verify(clientServer,times(0)).start();
    }

    @Test
    public void getAddressLog() {
        String addressLog = debugDB.getAddressLog();
        assertNotNull(addressLog);
    }

    @Test
    public void shutDown() throws IllegalAccessException {
        field(DebugDB.class, "clientServer").set(debugDB, new ClientServer(context,1,dbFactory));
        debugDB.shutDown();
        verify(clientServer,times(0)).stop();
    }

    @Test
    public void setCustomDatabaseFiles() {
        debugDB.setCustomDatabaseFiles(new HashMap<>());
        verify(clientServer,times(0)).setCustomDatabaseFiles(any());
    }

    @Test
    public void setInMemoryRoomDatabases() {
        debugDB.setInMemoryRoomDatabases(new HashMap<>());
        verify(clientServer,times(0)).setInMemoryRoomDatabases(any());
    }

    @Test
    public void isServerRunning() {
        boolean serverRunning = debugDB.isServerRunning();
        assertNotNull(serverRunning);
    }
}