package com.amitshekhar.utils;

import ohos.app.Context;
import ohos.wifi.WifiDevice;
import ohos.wifi.WifiLinkedInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Optional;
import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest({WifiDevice.class, Optional.class,Logger.class,NetworkUtils.class,WifiLinkedInfo.class})
@RunWith(PowerMockRunner.class)
public class NetworkUtilsTest {
    Context context;
    @Before
    public void setUp() throws Exception {
        context = mock(Context.class);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getAddressLog() {
        mockStatic(WifiDevice.class);
        WifiDevice wifiDevice = mock(WifiDevice.class);
        when(WifiDevice.getInstance(context)).thenReturn(wifiDevice);
        Optional<WifiLinkedInfo> ipInfo = mock(Optional.class);
        when(wifiDevice.getLinkedInfo()).thenReturn(ipInfo);
        when(ipInfo.get()).thenReturn(mock(WifiLinkedInfo.class));
        String addressLog = NetworkUtils.getAddressLog(context, 80);
        assertNotNull(addressLog);
    }
}