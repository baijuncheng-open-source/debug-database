package com.amitshekhar.utils;

import com.amitshekhar.model.Response;
import com.amitshekhar.model.RowDataRequest;
import com.amitshekhar.model.TableDataResponse;
import com.amitshekhar.model.UpdateRowResponse;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anySet;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@PrepareForTest({PrefHelper.class, DatabaseHelper.class, Preferences.class})
@RunWith(PowerMockRunner.class)
public class PrefHelperTest {
    Context context;
    List<RowDataRequest> rowDataRequests;
    @Before
    public void setUp() throws Exception {
        context = mock(Context.class);

        rowDataRequests = new ArrayList<>();
        RowDataRequest dataRequest1 = new RowDataRequest();
        RowDataRequest dataRequest2 = new RowDataRequest();

        dataRequest1.dataType = DataType.INTEGER;
        dataRequest2.dataType = DataType.LONG;

        dataRequest1.value = "11";
        dataRequest2.value = "22";

        dataRequest1.isPrimary = true;
        dataRequest2.isPrimary = false;

        rowDataRequests.add(dataRequest1);
        rowDataRequests.add(dataRequest2);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = NullPointerException.class)
    public void getSharedPreferenceTags() throws Exception {
        when(context.getApplicationContext()).thenReturn(mock(Context.class));
        when(context.getPreferencesDir()).thenReturn(mock(File.class));
        when(mock(File.class).getAbsolutePath()).thenReturn("../");

        List<String> sharedPreferenceTags = PrefHelper.getSharedPreferenceTags(context);
        assertNotNull(sharedPreferenceTags);
    }

    @Test(expected = NullPointerException.class)
    public void getAllPrefTableName() {
        Response allPrefTableName = PrefHelper.getAllPrefTableName(context);
        assertNotNull(allPrefTableName);
    }

    @Test
    public void getAllPrefDataWithInteger() throws Exception {
        when(context.getApplicationContext()).thenReturn(mock(Context.class));
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withAnyArguments().thenReturn(databaseHelper);

        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("11.xml")).thenReturn(preferences);
        Map<String, Integer> allEntries = new HashMap<>();
        allEntries.put("k1",1111);
        allEntries.put("k2",1222);
        when(preferences.getAll()).thenReturn((Map)allEntries);
        TableDataResponse allPrefData = PrefHelper.getAllPrefData(context, "11");
        assertNotNull(allPrefData);
    }

    @Test
    public void getAllPrefDataWithString() throws Exception {
        when(context.getApplicationContext()).thenReturn(mock(Context.class));
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withAnyArguments().thenReturn(databaseHelper);

        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("11.xml")).thenReturn(preferences);
        Map<String, String> allEntries = new HashMap<>();
        allEntries.put("k1","1111");
        allEntries.put("k2","1222");
        when(preferences.getAll()).thenReturn((Map)allEntries);
        TableDataResponse allPrefData = PrefHelper.getAllPrefData(context, "11");
        assertNotNull(allPrefData);
    }

    @Test
    public void getAllPrefDataWithLong() throws Exception {
        when(context.getApplicationContext()).thenReturn(mock(Context.class));
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withAnyArguments().thenReturn(databaseHelper);

        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("11.xml")).thenReturn(preferences);
        Map<String, Long> allEntries = new HashMap<>();
        allEntries.put("k1",1111L);
        allEntries.put("k2",2222L);
        when(preferences.getAll()).thenReturn((Map)allEntries);
        TableDataResponse allPrefData = PrefHelper.getAllPrefData(context, "11");
        assertNotNull(allPrefData);
    }

    @Test
    public void getAllPrefDataWithFloat() throws Exception {
        when(context.getApplicationContext()).thenReturn(mock(Context.class));
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withAnyArguments().thenReturn(databaseHelper);

        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("11.xml")).thenReturn(preferences);
        Map<String, Float> allEntries = new HashMap<>();
        allEntries.put("k1",1111f);
        allEntries.put("k2",2222f);
        when(preferences.getAll()).thenReturn((Map)allEntries);
        TableDataResponse allPrefData = PrefHelper.getAllPrefData(context, "11");
        assertNotNull(allPrefData);
    }

    @Test
    public void getAllPrefDataWithBoolean() throws Exception {
        when(context.getApplicationContext()).thenReturn(mock(Context.class));
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withAnyArguments().thenReturn(databaseHelper);

        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("11.xml")).thenReturn(preferences);
        Map<String, Boolean> allEntries = new HashMap<>();
        allEntries.put("k1",true);
        allEntries.put("k2",false);
        when(preferences.getAll()).thenReturn((Map)allEntries);
        TableDataResponse allPrefData = PrefHelper.getAllPrefData(context, "11");
        assertNotNull(allPrefData);
    }

    @Test
    public void getAllPrefDataWithSet() throws Exception {
        when(context.getApplicationContext()).thenReturn(mock(Context.class));
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withAnyArguments().thenReturn(databaseHelper);

        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("11.xml")).thenReturn(preferences);
        Map<String, Set> allEntries = new HashMap<>();
        allEntries.put("k1",new HashSet(11));
        allEntries.put("k2",new HashSet(22));
        when(preferences.getAll()).thenReturn((Map)allEntries);
        TableDataResponse allPrefData = PrefHelper.getAllPrefData(context, "22");
        assertNotNull(allPrefData);
    }


    @Test
    public void addOrUpdateRowWhenLong() throws Exception {
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withArguments(context).thenReturn(databaseHelper);
        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("users")).thenReturn(preferences);
        when(preferences.putLong(anyString(),anyLong())).thenReturn(preferences);
        when(preferences.putFloat(anyString(),anyFloat())).thenReturn(preferences);
        when(preferences.putString(anyString(),anyString())).thenReturn(preferences);
        when(preferences.putInt(anyString(),anyInt())).thenReturn(preferences);
        when(preferences.putBoolean(anyString(),anyBoolean())).thenReturn(preferences);
        when(preferences.putStringSet(anyString(),anySet())).thenReturn(preferences);

        UpdateRowResponse users = PrefHelper.addOrUpdateRow(context, "users", rowDataRequests);
        assertEquals(users.isSuccessful,true);
    }

    @Test
    public void addOrUpdateRowWhenInt() throws Exception {
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withArguments(context).thenReturn(databaseHelper);
        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("users")).thenReturn(preferences);
        when(preferences.putLong(anyString(),anyLong())).thenReturn(preferences);
        when(preferences.putFloat(anyString(),anyFloat())).thenReturn(preferences);
        when(preferences.putString(anyString(),anyString())).thenReturn(preferences);
        when(preferences.putInt(anyString(),anyInt())).thenReturn(preferences);
        when(preferences.putBoolean(anyString(),anyBoolean())).thenReturn(preferences);
        when(preferences.putStringSet(anyString(),anySet())).thenReturn(preferences);

        List<RowDataRequest> rowDataRequestsInt = new ArrayList<>();
        RowDataRequest dataRequest1 = new RowDataRequest();
        RowDataRequest dataRequest2 = new RowDataRequest();

        dataRequest1.dataType = DataType.INTEGER;
        dataRequest2.dataType = DataType.INTEGER;

        dataRequest1.value = "11";
        dataRequest2.value = "22";

        dataRequest1.isPrimary = true;
        dataRequest2.isPrimary = false;

        rowDataRequestsInt.add(dataRequest1);
        rowDataRequestsInt.add(dataRequest2);

        UpdateRowResponse users = PrefHelper.addOrUpdateRow(context, "users", rowDataRequestsInt);
        assertEquals(users.isSuccessful,true);
    }

    @Test
    public void addOrUpdateRowWhenText() throws Exception {
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withArguments(context).thenReturn(databaseHelper);
        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("users")).thenReturn(preferences);
        when(preferences.putLong(anyString(),anyLong())).thenReturn(preferences);
        when(preferences.putFloat(anyString(),anyFloat())).thenReturn(preferences);
        when(preferences.putString(anyString(),anyString())).thenReturn(preferences);
        when(preferences.putInt(anyString(),anyInt())).thenReturn(preferences);
        when(preferences.putBoolean(anyString(),anyBoolean())).thenReturn(preferences);
        when(preferences.putStringSet(anyString(),anySet())).thenReturn(preferences);

        List<RowDataRequest> rowDataRequestsText = new ArrayList<>();
        RowDataRequest dataRequest1 = new RowDataRequest();
        RowDataRequest dataRequest2 = new RowDataRequest();

        dataRequest1.dataType = DataType.TEXT;
        dataRequest2.dataType = DataType.TEXT;

        dataRequest1.value = "11";
        dataRequest2.value = "22";

        dataRequest1.isPrimary = true;
        dataRequest2.isPrimary = false;

        rowDataRequestsText.add(dataRequest1);
        rowDataRequestsText.add(dataRequest2);

        UpdateRowResponse users = PrefHelper.addOrUpdateRow(context, "users", rowDataRequestsText);
        assertEquals(users.isSuccessful,true);
    }

    @Test
    public void addOrUpdateRowWhenFloat() throws Exception {
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withArguments(context).thenReturn(databaseHelper);
        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("users")).thenReturn(preferences);
        when(preferences.putLong(anyString(),anyLong())).thenReturn(preferences);
        when(preferences.putFloat(anyString(),anyFloat())).thenReturn(preferences);
        when(preferences.putString(anyString(),anyString())).thenReturn(preferences);
        when(preferences.putInt(anyString(),anyInt())).thenReturn(preferences);
        when(preferences.putBoolean(anyString(),anyBoolean())).thenReturn(preferences);
        when(preferences.putStringSet(anyString(),anySet())).thenReturn(preferences);

        List<RowDataRequest> rowDataRequestsFloat = new ArrayList<>();
        RowDataRequest dataRequest1 = new RowDataRequest();
        RowDataRequest dataRequest2 = new RowDataRequest();

        dataRequest1.dataType = DataType.FLOAT;
        dataRequest2.dataType = DataType.FLOAT;

        dataRequest1.value = "11";
        dataRequest2.value = "22";

        dataRequest1.isPrimary = true;
        dataRequest2.isPrimary = false;

        rowDataRequestsFloat.add(dataRequest1);
        rowDataRequestsFloat.add(dataRequest2);

        UpdateRowResponse users = PrefHelper.addOrUpdateRow(context, "users", rowDataRequestsFloat);
        assertEquals(users.isSuccessful,true);
    }

    @Test
    public void addOrUpdateRowWhenBoolean() throws Exception {
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withArguments(context).thenReturn(databaseHelper);
        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("users")).thenReturn(preferences);
        when(preferences.putLong(anyString(),anyLong())).thenReturn(preferences);
        when(preferences.putFloat(anyString(),anyFloat())).thenReturn(preferences);
        when(preferences.putString(anyString(),anyString())).thenReturn(preferences);
        when(preferences.putInt(anyString(),anyInt())).thenReturn(preferences);
        when(preferences.putBoolean(anyString(),anyBoolean())).thenReturn(preferences);
        when(preferences.putStringSet(anyString(),anySet())).thenReturn(preferences);

        List<RowDataRequest> rowDataRequestsBoolean = new ArrayList<>();
        RowDataRequest dataRequest1 = new RowDataRequest();
        RowDataRequest dataRequest2 = new RowDataRequest();

        dataRequest1.dataType = DataType.BOOLEAN;
        dataRequest2.dataType = DataType.BOOLEAN;

        dataRequest1.value = "11";
        dataRequest2.value = "22";

        dataRequest1.isPrimary = true;
        dataRequest2.isPrimary = false;

        rowDataRequestsBoolean.add(dataRequest1);
        rowDataRequestsBoolean.add(dataRequest2);

        UpdateRowResponse users = PrefHelper.addOrUpdateRow(context, "users", rowDataRequestsBoolean);
        assertEquals(users.isSuccessful,true);
    }

    @Test
    public void addOrUpdateRowWhenStringSet() throws Exception {
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withArguments(context).thenReturn(databaseHelper);
        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("users")).thenReturn(preferences);
        when(preferences.putLong(anyString(),anyLong())).thenReturn(preferences);
        when(preferences.putFloat(anyString(),anyFloat())).thenReturn(preferences);
        when(preferences.putString(anyString(),anyString())).thenReturn(preferences);
        when(preferences.putInt(anyString(),anyInt())).thenReturn(preferences);
        when(preferences.putBoolean(anyString(),anyBoolean())).thenReturn(preferences);
        when(preferences.putStringSet(anyString(),anySet())).thenReturn(preferences);

        List<RowDataRequest> rowDataRequestsStringSet = new ArrayList<>();
        RowDataRequest dataRequest1 = new RowDataRequest();
        RowDataRequest dataRequest2 = new RowDataRequest();

        dataRequest1.dataType = DataType.STRING_SET;
        dataRequest2.dataType = DataType.STRING_SET;

        dataRequest1.value = "11";
        dataRequest2.value = "22";

        dataRequest1.isPrimary = true;
        dataRequest2.isPrimary = false;

        rowDataRequestsStringSet.add(dataRequest1);
        rowDataRequestsStringSet.add(dataRequest2);

        UpdateRowResponse users = PrefHelper.addOrUpdateRow(context, "users", rowDataRequestsStringSet);
        assertEquals(users.isSuccessful,false);
    }

    @Test
    public void addOrUpdateRowWhenTableNameIsNull() {
        UpdateRowResponse updateRowResponse = PrefHelper.addOrUpdateRow(context, null, rowDataRequests);
        assertEquals(updateRowResponse.isSuccessful,false);
    }

    @Test
    public void deleteRow() throws Exception {
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withArguments(context).thenReturn(databaseHelper);
        UpdateRowResponse users = PrefHelper.deleteRow(context, "users", rowDataRequests);
        assertEquals(users.isSuccessful,false);
    }

    @Test
    public void deleteRowWhenTableNameIsNull() throws Exception {
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withArguments(context).thenReturn(databaseHelper);
        UpdateRowResponse updateRowResponse = PrefHelper.deleteRow(context, null, rowDataRequests);
        assertEquals(updateRowResponse.isSuccessful,false);
    }
}