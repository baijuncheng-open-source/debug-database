package com.amitshekhar.utils;

import ohos.agp.utils.TextTool;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.utils.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.*;

@PrepareForTest({TextTool.class, ResourceManager.class, RawFileEntry.class, Resource.class,Pair.class,Utils.class})
@RunWith(PowerMockRunner.class)
public class UtilsTest {

    @Before
    public void setUp() throws Exception {
        mockStatic(TextTool.class);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void detectMimeTypeIsNull() {
        when(TextTool.isNullOrEmpty(null)).thenReturn(true);
        String type = Utils.detectMimeType(null);
        assertEquals(type,null);
    }

    @Test
    public void detectMimeType1() {
        String type = Utils.detectMimeType("aa.html");
        assertEquals(type,"text/html");
    }

    @Test
    public void detectMimeType2() {
        String type = Utils.detectMimeType("aa.css");
        assertEquals(type,"text/css");
    }

    @Test
    public void detectMimeType3() {
        String type = Utils.detectMimeType("aa.js");
        assertEquals(type,"application/javascript");
    }

    @Test
    public void detectMimeType4() {
        String type = Utils.detectMimeType("aa.txt");
        assertEquals(type,"application/octet-stream");
    }

    @Test
    public void loadContent() throws Exception {
        String fileName = "../../../../resources/rawfile/index.html";
        ResourceManager resourceManager = mock(ResourceManager.class);
        RawFileEntry rawFileEntry = mock(RawFileEntry.class);
        when(resourceManager.getRawFileEntry(fileName)).thenReturn(rawFileEntry);

        Resource inputStream = mock(Resource.class);
        when(inputStream.read(any())).thenReturn(0).thenReturn(-1);
        when(rawFileEntry.openRawFile()).thenReturn(inputStream);
        byte[] bytes = Utils.loadContent(fileName, resourceManager);
        assertNotNull(bytes);
    }

    @Test
    public void getDatabase() throws Exception {
        FileInputStream inputStream=mock(FileInputStream.class);
        whenNew(FileInputStream.class).withAnyArguments().thenReturn(inputStream);
        when(inputStream.read(any())).thenReturn(100).thenReturn(-1);
        ByteArrayOutputStream bos = mock(ByteArrayOutputStream.class);
        whenNew(ByteArrayOutputStream.class).withNoArguments().thenReturn(bos);
        HashMap<String, Pair<File, String>> databaseFiles = new HashMap<>();
        Pair<File, String> pair = mock(Pair.class);
        Whitebox.setInternalState(pair,"f",new File("/path"));
        databaseFiles.put("users",pair);
        when(bos.toByteArray()).thenReturn(new byte[]{'1','2'});
        byte[] users = Utils.getDatabase("users", databaseFiles);
        assertNotNull(users);
    }

    @Test
    public void getDatabaseWhenDatabaseIsNull() {
        HashMap<String, Pair<File, String>> databaseFiles = new HashMap<>();
        byte[] users = Utils.getDatabase("users", databaseFiles);
        assertNull(users);
    }
}