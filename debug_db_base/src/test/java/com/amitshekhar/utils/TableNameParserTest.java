package com.amitshekhar.utils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collection;
import java.util.regex.Matcher;

import static org.junit.Assert.assertNotNull;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest({TableNameParser.class})
@RunWith(PowerMockRunner.class)
public class TableNameParserTest {

    TableNameParser tableNameParser;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void tablesWhenSelect() {
        tableNameParser = new TableNameParser("select * from user ");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }

    @Test
    public void tablesWhenDelete() {
        tableNameParser = new TableNameParser("delete from user where a = b ");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }

    @Test
    public void tablesWhenDelete2() {
        tableNameParser = new TableNameParser("/*+ delete from user where .../*+ ");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }

    @Test
    public void tablesWhenDelete3() {
        tableNameParser = new TableNameParser("*/ delete from user where */ ");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }

    @Test
    public void tablesWhenDelete4() {
        tableNameParser = new TableNameParser("*/ delete from user where */ ");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }

    @Test
    public void tablesWhenDelete5() {
        Matcher matcher = mock(Matcher.class);
        when(matcher.find()).thenReturn(true);
        tableNameParser = new TableNameParser(" -- delete from  user where -- ;");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }

    @Test
    public void tablesWhenDeleteTable() {
        tableNameParser = new TableNameParser("delete users;");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }

    @Test
    public void tablesWhenCreate() {
        tableNameParser = new TableNameParser("create table books{};");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }

    @Test
    public void tablesWhenJoin() {
        tableNameParser = new TableNameParser("SELECT last_name,job_title\n" +
                "\tFROM employees e\n" +
                "\tINNER JOIN jobs j\n" +
                "\tON e.`job_id`=  j.`job_id`\n" +
                "\tWHERE e.`last_name` LIKE '%e%';");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }

    @Test
    public void tablesWhenShow() {
        tableNameParser = new TableNameParser("show index from books");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }
    @Test
    public void tablesWhenUpdate() {
        tableNameParser = new TableNameParser("UPDATE boys ,");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }

    @Test
    public void tablesWhenCreateIndex() {
        tableNameParser = new TableNameParser("create index PersonIndex on Person ;");
        Collection<String> tables = tableNameParser.tables();
        assertNotNull(tables);
    }
}