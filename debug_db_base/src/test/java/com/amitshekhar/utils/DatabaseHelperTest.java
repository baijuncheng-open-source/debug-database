package com.amitshekhar.utils;

import com.amitshekhar.model.Response;
import com.amitshekhar.model.RowDataRequest;
import com.amitshekhar.model.TableDataResponse;
import com.amitshekhar.model.UpdateRowResponse;
import com.amitshekhar.sqlite.SQLiteDB;
import ohos.agp.utils.TextTool;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static ohos.data.resultset.ResultSet.ColumnType.TYPE_BLOB;
import static ohos.data.resultset.ResultSet.ColumnType.TYPE_FLOAT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@PrepareForTest({DatabaseHelper.class, Logger.class,ResultSet.class, TextTool.class,SQLiteDB.class})
@RunWith(PowerMockRunner.class)
public class DatabaseHelperTest {
    List<RowDataRequest> rowDataRequests;

    @Before
    public void setUp() throws Exception {
        mockStatic(TextTool.class);
        rowDataRequests = new ArrayList<>();
        RowDataRequest dataRequest1 = new RowDataRequest();
        RowDataRequest dataRequest2 = new RowDataRequest();
        RowDataRequest dataRequest3 = new RowDataRequest();
        dataRequest1.dataType = DataType.INTEGER;
        dataRequest2.dataType = DataType.REAL;
        dataRequest3.dataType = DataType.TEXT;
        dataRequest1.value = "11";
        dataRequest2.value = "22";
        dataRequest3.value = "33";
        dataRequest1.isPrimary = true;
        dataRequest2.isPrimary = false;
        dataRequest3.isPrimary = true;
        rowDataRequests.add(dataRequest1);
        rowDataRequests.add(dataRequest2);
        rowDataRequests.add(dataRequest3);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getAllTableName() {
        SQLiteDB database = mock(SQLiteDB.class);
        ResultSet c = mock(ResultSet.class);
        when(database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' " +
                "OR type='view' ORDER BY name COLLATE NOCASE", null)).thenReturn(c);
        when(c.getRowCount()).thenReturn(1);
        when(c.isAtFirstRow()).thenReturn(true);
        when(c.goToFirstRow()).thenReturn(true);
        when(c.isAtLastRow()).thenReturn(true);
        when(c.isEnded()).thenReturn(false).thenReturn(true);
        Response allTableName = DatabaseHelper.getAllTableName(database);
        assertNotNull(allTableName);

    }

    @Test
    public void getTableData() {
        SQLiteDB db = mock(SQLiteDB.class);
        ResultSet cursor = mock(ResultSet.class);
        when(db.rawQuery("SELECT type FROM sqlite_master WHERE name=?",new String[]{"[users]"})).thenReturn(cursor);
        when(cursor.goToFirstRow()).thenReturn(true);
        when(db.rawQuery("SELECT name FROM [users] WHERE type='table'", null)).thenReturn(cursor);

        when(cursor.getColumnCount()).thenReturn(2);
        when(cursor.getAllColumnNames()).thenReturn(new String[]{"aa","bb","cc","dd"});
        when(cursor.getRowCount()).thenReturn(3);
        when(cursor.getColumnTypeForIndex(0)).thenReturn(TYPE_BLOB);
        when(cursor.getColumnTypeForIndex(1)).thenReturn(TYPE_FLOAT);
        when(cursor.getBlob(anyInt())).thenReturn(new byte[]{'a','b'});

        TableDataResponse users = DatabaseHelper.getTableData(db, "SELECT name FROM users WHERE type='table'", "users");
        assertNotNull(users);
    }

    @Test
    public void getTableDataWhenTableNameIsNull() {
        SQLiteDB db = mock(SQLiteDB.class);
        ResultSet cursor = mock(ResultSet.class);
        when(db.rawQuery("SELECT type FROM sqlite_master WHERE name=?",new String[]{"users"})).thenReturn(cursor);
        when(cursor.goToFirstRow()).thenReturn(true);
        when(cursor.getColumnCount()).thenReturn(2);
        TableDataResponse tableData = DatabaseHelper.getTableData(db, "SELECT name FROM users WHERE type='table' ", null);
        assertNotNull(tableData);

    }

    @Test
    public void addRow() throws Exception {
        SQLiteDB db = mock(SQLiteDB.class);
        ValuesBucket contentValues = mock(ValuesBucket.class);
        whenNew(ValuesBucket.class).withNoArguments().thenReturn(contentValues);
        UpdateRowResponse users = DatabaseHelper.addRow(db, "users", rowDataRequests);
        assertEquals(users.isSuccessful,false);
    }

    @Test
    public void addRowWhenTableNameIsNull() throws Exception {
        SQLiteDB db = mock(SQLiteDB.class);
        List<RowDataRequest> rowDataRequests = new ArrayList<>();
        ValuesBucket contentValues = mock(ValuesBucket.class);
        whenNew(ValuesBucket.class).withNoArguments().thenReturn(contentValues);
        UpdateRowResponse updateRowResponse = DatabaseHelper.addRow(db, null, rowDataRequests);
        assertEquals(updateRowResponse.isSuccessful,false);
    }

    @Test
    public void updateRow() throws Exception {
        SQLiteDB db = mock(SQLiteDB.class);
        ValuesBucket contentValues = mock(ValuesBucket.class);
        whenNew(ValuesBucket.class).withNoArguments().thenReturn(contentValues);
        UpdateRowResponse users = DatabaseHelper.updateRow(db, "users", rowDataRequests);
        assertEquals(users.isSuccessful,true);
    }

    @Test
    public void updateRowWhenTableNameIsNull() throws Exception {
        SQLiteDB db = mock(SQLiteDB.class);
        ValuesBucket contentValues = mock(ValuesBucket.class);
        whenNew(ValuesBucket.class).withNoArguments().thenReturn(contentValues);
        UpdateRowResponse updateRowResponse = DatabaseHelper.updateRow(db, null, rowDataRequests);
        assertEquals(updateRowResponse.isSuccessful,false);
    }

    @Test
    public void deleteRow() {
        SQLiteDB db = mock(SQLiteDB.class);
        UpdateRowResponse users = DatabaseHelper.deleteRow(db, "users", rowDataRequests);
        assertEquals(users.isSuccessful,true);
    }

    @Test
    public void deleteRowWhenTableNameIsNull() {
        SQLiteDB db = mock(SQLiteDB.class);
        UpdateRowResponse updateRowResponse = DatabaseHelper.deleteRow(db, null, rowDataRequests);
        assertEquals(updateRowResponse.isSuccessful,false);
    }

    @Test
    public void exec() {
        SQLiteDB database = mock(SQLiteDB.class);
        TableDataResponse users = DatabaseHelper.exec(database, "select type from users");
        assertEquals(users.isSuccessful,true);
    }
}