package com.amitshekhar.utils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@PrepareForTest({})
@RunWith(PowerMockRunner.class)
public class ConverterUtilsTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void blobToString() {
        String s = ConverterUtils.blobToString(new byte[]{1, 2});
        assertNotNull(s);
    }

    @Test
    public void fastIsAscii() {
        boolean b = ConverterUtils.fastIsAscii(new byte[]{1, 2});
        assertEquals(b,true);
    }

    @Test
    public void fastIsAsciiWithNull() {
        boolean b = ConverterUtils.fastIsAscii(new byte[]{-1, 1});
        assertEquals(b,false);
    }
}