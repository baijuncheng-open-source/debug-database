package com.amitshekhar.utils;

import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.utils.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.util.HashMap;

import static org.junit.Assert.assertNotNull;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;

@PrepareForTest({DatabaseFileProvider.class, TextTool.class,File.class})
@RunWith(PowerMockRunner.class)
public class DatabaseFileProviderTest {

    @Before
    public void setUp() throws Exception {
        mockStatic(TextTool.class);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getDatabaseFiles() throws Exception {
        HashMap<String, Pair<File, String>> databaseFiles = new HashMap<>();
        whenNew(HashMap.class).withNoArguments().thenReturn(databaseFiles);
        Context context = mock(Context.class);
        when(context.getApplicationContext()).thenReturn(context);
        File databaseDir = new File("../../");
        System.out.println("path:"+databaseDir.getAbsolutePath());
        when(context.getApplicationContext().getDatabaseDir()).thenReturn(databaseDir);

        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withAnyArguments().thenReturn(databaseHelper);
        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences("rdb_info")).thenReturn(preferences);
        when(preferences.getString("aaa", "")).thenReturn("123456");

        Pair pair = mock(Pair.class);
        whenNew(Pair.class).withAnyArguments().thenReturn(pair);

        when(TextTool.isNullOrEmpty(null)).thenReturn(true);

        HashMap<String, Pair<File, String>> map = DatabaseFileProvider.getDatabaseFiles(context);
        assertNotNull(map);
    }
}