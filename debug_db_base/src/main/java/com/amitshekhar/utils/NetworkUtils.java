/*
 *
 *  *    Copyright (C) 2019 Amit Shekhar
 *  *    Copyright (C) 2011 ohos Open Source Project
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.amitshekhar.utils;

import ohos.app.Context;
import ohos.wifi.WifiDevice;
import ohos.wifi.WifiLinkedInfo;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by amitshekhar on 15/11/16.
 */

public final class NetworkUtils {

    private static final String TAG = "NetworkUtils";

    private NetworkUtils() {
        // This class in not publicly instantiable
    }

    public static String getAddressLog(Context context, int port) {
        WifiDevice wifiDevice = WifiDevice.getInstance(context);
        Optional<WifiLinkedInfo> ipInfo = wifiDevice.getLinkedInfo();
        Logger.getLogger(TAG).log(Level.INFO,ipInfo.get().toString());
        int ipAddress = ipInfo.get().getIpAddress();
        Logger.getLogger(TAG).log(Level.INFO,String.valueOf(ipAddress));
        final String formattedIpAddress = String.format("%d.%d.%d.%d",
                (ipAddress & 0xff),
                (ipAddress >> 8 & 0xff),
                (ipAddress >> 16 & 0xff),
                (ipAddress >> 24 & 0xff));
        return "Open http://" + formattedIpAddress + ":" + port + " in your browser";
    }

}
