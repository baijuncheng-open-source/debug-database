/*
 *
 *  *    Copyright (C) 2019 Amit Shekhar
 *  *    Copyright (C) 2011 ohos Open Source Project
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */
package com.amitshekhar.utils;


import com.amitshekhar.DebugDB;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.utils.Pair;

import java.io.File;
import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.HashMap;

/**
 * Created by amitshekhar on 06/02/17.
 */

public class DatabaseFileProvider {

    private static final String DB_PASSWORD_RESOURCE = "DB_PASSWORD_{0}";
    private static final String TAG = "DatabaseFileProvider";
    public static final String NAME_PREFS = "rdb_info";

    private DatabaseFileProvider() {
    }

    public static HashMap<String, Pair<File, String>> getDatabaseFiles(Context context) {
        HashMap<String, Pair<File, String>> databaseFiles = new HashMap<>();
        try {
            File databaseDir = context.getApplicationContext().getDatabaseDir();
            File[] files = databaseDir.listFiles();
            for (File f:files) {
                if (f.exists() && f.isDirectory()) {
                    String[] databaseList = f.list();
                    for (String databaseName : databaseList) {
                        String password = getDbPasswordFromStringResources(context, databaseName);
                        databaseFiles.put(databaseName, new Pair<File, String>(getDatabasePath(f, databaseName), password));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return databaseFiles;
    }

    private static File getDatabasePath(File file, String name) {
        File dir;
        File f;

        if (name.charAt(0) == File.separatorChar) {
            String dirPath = name.substring(0, name.lastIndexOf(File.separatorChar));
            dir = new File(dirPath);
            name = name.substring(name.lastIndexOf(File.separatorChar));
            f = new File(dir, name);
        } else {
            f = makeFilename(file, name);
        }

        return f;
    }

    private static File makeFilename(File base, String name) {
        if (name.indexOf(File.separatorChar) < 0) {
            return new File(base, name);
        }
        throw new IllegalArgumentException(
                "File " + name + " contains a path separator");
    }

    private static String getDbPasswordFromStringResources(Context context, String name) {
        String nameWithoutExt = name;
        if (nameWithoutExt.endsWith(".db")) {
            nameWithoutExt = nameWithoutExt.substring(0, nameWithoutExt.lastIndexOf('.'));
        }
        String resourceName = MessageFormat.format(DB_PASSWORD_RESOURCE, nameWithoutExt.toUpperCase());
        String password = "";

        DatabaseHelper databaseHelper = new DatabaseHelper(context.getApplicationContext());
        Preferences preferences = databaseHelper.getPreferences(NAME_PREFS);
        password = preferences.getString(resourceName, "");
        if (TextTool.isNullOrEmpty(password)) {
            Class<DebugDB> clazz = DebugDB.class;
            try {
                Field declaredField = clazz.getDeclaredField(resourceName);
                declaredField.setAccessible(true);
                password = (String) declaredField.get(null);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return password;
    }
}
