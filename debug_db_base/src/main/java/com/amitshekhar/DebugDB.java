/*
 *
 *  *    Copyright (C) 2019 Amit Shekhar
 *  *    Copyright (C) 2011 ohos Open Source Project
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.amitshekhar;

import com.amitshekhar.server.ClientServer;
import com.amitshekhar.sqlite.DBFactory;
import com.amitshekhar.utils.NetworkUtils;
import ohos.app.Context;
import ohos.data.rdb.RdbStore;
import ohos.utils.Pair;

import java.io.File;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by amitshekhar on 15/11/16.
 */

public class DebugDB {

    private static final String TAG = DebugDB.class.getSimpleName();
    private static final int DEFAULT_PORT = 8080;
    private static final String DB_PASSWORD_PERSON = "a_password";
    private static ClientServer clientServer;
    private static String addressLog = "not available";

    private DebugDB() {
        // This class in not publicly instantiable
    }

    public static void initialize(Context context, DBFactory dbFactory) {
        int portNumber;

        try {
            portNumber = DEFAULT_PORT;
        } catch (NumberFormatException ex) {
            Logger.getLogger(TAG).log(Level.SEVERE,"PORT_NUMBER should be integer", ex);
            portNumber = DEFAULT_PORT;
            Logger.getLogger(TAG).log(Level.INFO,"Using Default port : " + DEFAULT_PORT);
        }

        clientServer = new ClientServer(context, portNumber, dbFactory);
        clientServer.start();
        addressLog = NetworkUtils.getAddressLog(context, portNumber);
        Logger.getLogger(TAG).log(Level.INFO,addressLog);
    }

    public static String getAddressLog() {
        Logger.getLogger(TAG).log(Level.INFO,addressLog);
        return addressLog;
    }

    public static void shutDown() {
        if (clientServer != null) {
            clientServer.stop();
            clientServer = null;
        }
    }

    public static void setCustomDatabaseFiles(HashMap<String, Pair<File, String>> customDatabaseFiles) {
        if (clientServer != null) {
            clientServer.setCustomDatabaseFiles(customDatabaseFiles);
        }
    }

    public static void setInMemoryRoomDatabases(HashMap<String, RdbStore> databases) {
        if (clientServer != null) {
            clientServer.setInMemoryRoomDatabases(databases);
        }
    }

    public static boolean isServerRunning() {
        return clientServer != null && clientServer.isRunning();
    }

}
