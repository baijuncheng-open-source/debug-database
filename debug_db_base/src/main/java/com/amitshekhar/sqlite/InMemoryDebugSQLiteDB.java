package com.amitshekhar.sqlite;

import ohos.agp.utils.TextTool;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.rdb.RdbException;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;

import java.util.Arrays;
import java.util.Set;

/**
 * Created by anandgaurav on 12/02/18.
 */

public class InMemoryDebugSQLiteDB implements SQLiteDB {

    private final RdbStore database;

    public InMemoryDebugSQLiteDB(RdbStore database) {
        this.database = database;
    }

    public void beginTransaction() {
        database.beginTransaction();
    }

    public void endTransaction() {
        database.markAsCommit();
        database.endTransaction();
    }

    public RdbStore getRealDb() {
        return database;
    }

    @Override
    public int delete(String table, String whereClause, String[] whereArgs) {
        RawRdbPredicates rdbPredicates = new RawRdbPredicates(table);
        rdbPredicates.setWhereClause(whereClause);
        rdbPredicates.setWhereArgs(Arrays.asList(whereArgs));
        return database.delete(rdbPredicates);
    }

    @Override
    public boolean isOpen() {
        return database.isOpen();
    }

    @Override
    public void close() {
        // no ops
    }

    @Override
    public ResultSet rawQuery(String sql, String[] selectionArgs) {
        return database.querySql(sql,selectionArgs);
    }

    @Override
    public void execSQL(String sql) throws RdbException {
        database.executeSql(sql);
    }

    @Override
    public long insert(String table, String nullColumnHack, ValuesBucket values) {
        Set<String> columnSet = values.getColumnSet();
        if (!TextTool.isNullOrEmpty(nullColumnHack)) {
            for (String s : columnSet) {
                if (!s.equals(nullColumnHack)) {
                    values.putNull(s);
                }
            }
        }
        return database.insert(table,values);
    }

    @Override
    public int update(String table, ValuesBucket values, String whereClause, String[] whereArgs) {
        RawRdbPredicates rdbPredicates = new RawRdbPredicates(table);
        rdbPredicates.setWhereClause(whereClause);
        rdbPredicates.setWhereArgs(Arrays.asList(whereArgs));
        return database.update(values,rdbPredicates);
    }

    @Override
    public int getVersion() {
        return database.getVersion();
    }
}
