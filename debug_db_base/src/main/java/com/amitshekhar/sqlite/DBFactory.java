package com.amitshekhar.sqlite;


import ohos.app.Context;

public interface DBFactory {

    SQLiteDB create(Context context, String path, String password);

}
