package com.amitshekhar.sqlite;

import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.data.rdb.RdbException;


/**
 * Created by anandgaurav on 12/02/18.
 */

public interface SQLiteDB {

    int delete(String table, String whereClause, String[] whereArgs);

    boolean isOpen();

    void close();

    ResultSet rawQuery(String sql, String[] selectionArgs);

    void execSQL(String sql) throws RdbException;

    long insert(String table, String nullColumnHack, ValuesBucket values);

    int update(String table, ValuesBucket values, String whereClause, String[] whereArgs);

    int getVersion();

}
