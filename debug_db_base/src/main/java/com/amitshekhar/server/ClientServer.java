/*
 *
 *  *    Copyright (C) 2019 Amit Shekhar
 *  *    Copyright (C) 2011 ohos Open Source Project
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.amitshekhar.server;

/**
 * Created by amitshekhar on 15/11/16.
 */

import com.amitshekhar.sqlite.DBFactory;
import ohos.app.Context;
import ohos.data.rdb.RdbStore;
import ohos.utils.Pair;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientServer implements Runnable {

    private static final String TAG = "ClientServer";

    private final int mPort;
    private final RequestHandler mRequestHandler;
    private boolean mIsRunning;
    private ServerSocket mServerSocket;

    public ClientServer(Context context, int port, DBFactory dbFactory) {
        mRequestHandler = new RequestHandler(context, dbFactory);
        mPort = port;
    }

    public void start() {
        mIsRunning = true;
        Logger.getLogger(TAG).log(Level.INFO,"client server start.");
        new Thread(this).start();
    }

    public void stop() {
        try {
            mIsRunning = false;
            if (null != mServerSocket) {
                mServerSocket.close();
                mServerSocket = null;
            }
        } catch (Exception e) {
            Logger.getLogger(TAG).log(Level.SEVERE,"Error closing the server socket.",e);
        }
    }

    @Override
    public void run() {
        try {
            Logger.getLogger(TAG).log(Level.INFO,"run in thread. port: " + mPort);
            mServerSocket = new ServerSocket(mPort);
            while (mIsRunning) {
                Logger.getLogger(TAG).log(Level.INFO,"listening ...");
                Socket socket = mServerSocket.accept();
                mRequestHandler.handle(socket);
                socket.close();
            }
        } catch (SocketException e) {
            // The server was stopped; ignore.
            Logger.getLogger(TAG).log(Level.SEVERE,"SocketException.",e);
        } catch (IOException e) {
            // todo
            Logger.getLogger(TAG).log(Level.SEVERE,"Web server error.",e);
        } catch (Exception ignore) {
            Logger.getLogger(TAG).log(Level.SEVERE,"Exception.",ignore);
        }
    }

    public void setCustomDatabaseFiles(HashMap<String, Pair<File, String>> customDatabaseFiles) {
        mRequestHandler.setCustomDatabaseFiles(customDatabaseFiles);
    }

    public void setInMemoryRoomDatabases(HashMap<String, RdbStore> databases) {
        mRequestHandler.setInMemoryRoomDatabases(databases);
    }

    public boolean isRunning() {
        return mIsRunning;
    }
}
