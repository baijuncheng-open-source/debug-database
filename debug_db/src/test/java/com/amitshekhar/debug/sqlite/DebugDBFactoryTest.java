package com.amitshekhar.debug.sqlite;

import com.amitshekhar.sqlite.SQLiteDB;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertNotNull;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({StoreConfig.class,DebugDBFactory.class,RdbOpenCallback.class,RdbStore.class})
public class DebugDBFactoryTest {
    DebugDBFactory debugDBFactory;
    @Before
    public void setUp() throws Exception {
        debugDBFactory = new DebugDBFactory();

    }

    @After
    public void tearDown() throws Exception {
    }


    @Test(expected = RuntimeException.class)
    public void create() throws Exception {
        Context context = mock(Context.class);
        Context applicationContext = context.getApplicationContext();
        when(context.getApplicationContext()).thenReturn(applicationContext);

        mockStatic(StoreConfig.class);
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);

        whenNew(DatabaseHelper.class).withArguments(applicationContext).thenReturn(databaseHelper);

        RdbOpenCallback rdbOpenCallback = mock(RdbOpenCallback.class);
        whenNew(RdbOpenCallback.class).withNoArguments().thenReturn(rdbOpenCallback);
        when(databaseHelper.getRdbStore(mock(StoreConfig.class),1,rdbOpenCallback)).thenReturn(mock(RdbStore.class));

        SQLiteDB sqLiteDB = debugDBFactory.create(context, "111", "111");
        assertNotNull(sqLiteDB);
    }

}