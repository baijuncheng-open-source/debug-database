package com.amitshekhar.debug.sqlite;

import ohos.agp.utils.TextTool;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.spy;

@PrepareForTest({DebugSQLiteDB.class,RawRdbPredicates.class,TextTool.class})
@RunWith(PowerMockRunner.class)
public class DebugSQLiteDBTest {
    DebugSQLiteDB debugSQLiteDB;
    RdbStore database;
    ValuesBucket valuesBucket;

    @Before
    public void setUp() throws Exception {
        mockStatic(TextTool.class);
        database = PowerMockito.mock(RdbStore.class);
        debugSQLiteDB = new DebugSQLiteDB(database);
        valuesBucket = PowerMockito.mock(ValuesBucket.class);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void delete() throws Exception {
        RawRdbPredicates rdbPredicates = mock(RawRdbPredicates.class);
        whenNew(RawRdbPredicates.class).withArguments(anyString()).thenReturn(rdbPredicates);
        doNothing().when(rdbPredicates).setWhereClause(anyString());
        List<String> list = new ArrayList<>();
        List spy = spy(list);
        doNothing().when(rdbPredicates).setWhereArgs(spy);
        int users = debugSQLiteDB.delete("users", "", new String[]{"1","2"});
        assertEquals(users,0);
    }

    @Test
    public void isOpen() {
        boolean open = debugSQLiteDB.isOpen();
        assertEquals(open,false);
    }

    @Test
    public void close() {
        debugSQLiteDB.close();
        verify(database,times(1)).close();
    }

    @Test
    public void rawQueryWithSql() {
        ResultSet rawQuery = debugSQLiteDB.rawQuery("select * from users", new String[]{"11","22"});
        assertEquals(rawQuery,null);
    }

    @Test
    public void rawQueryNoSql() {
        ResultSet rawQuery = debugSQLiteDB.rawQuery(null, null);
        assertEquals(rawQuery,null);
    }

    @Test
    public void execSQLWithSql() {
        debugSQLiteDB.execSQL("select * from users");
        verify(database,times(1)).executeSql(anyString());
    }

    @Test
    public void execSQLNoSql() {
        debugSQLiteDB.execSQL(null);
        verify(database,times(1)).executeSql(null);
    }

    @Test
    public void insert() {
        valuesBucket.putLong("id", 1L);
        valuesBucket.putString("name", "Mark");
        long insert = debugSQLiteDB.insert("users", "", valuesBucket);
        assertEquals(insert,0);
    }

    @Test
    public void update() throws Exception {
        RawRdbPredicates rdbPredicates = mock(RawRdbPredicates.class);
        whenNew(RawRdbPredicates.class).withArguments("users").thenReturn(rdbPredicates);
        doNothing().when(rdbPredicates).setWhereClause("");
        doNothing().when(rdbPredicates).setWhereArgs(new ArrayList<>());
        int update = debugSQLiteDB.update("users", valuesBucket, "", new String[]{"11", "22"});
        assertEquals(update,0);
    }

    @Test
    public void getVersion() {
        int version = debugSQLiteDB.getVersion();
        assertEquals(version,0);
    }
}