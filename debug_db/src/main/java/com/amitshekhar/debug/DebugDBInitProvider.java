/*
 *
 *  *    Copyright (C) 2019 Amit Shekhar
 *  *    Copyright (C) 2011 ohos Open Source Project
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.amitshekhar.debug;

import com.amitshekhar.DebugDB;
import com.amitshekhar.debug.sqlite.DebugDBFactory;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.bundle.AbilityInfo;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.PacMap;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by amitshekhar on 16/11/16.
 */

public class DebugDBInitProvider extends Ability {
    private DataAbilityHelper dataAbilityHelper;
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "DebugDBInitProvider");

    public DebugDBInitProvider() {
        dataAbilityHelper = DataAbilityHelper.creator(this);
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        HiLog.info(LABEL_LOG,"DebugDBInitProvider-onStart");
        attachInfo(this,getAbilityInfo());
        DebugDB.initialize(this, new DebugDBFactory());
    }

    public void attachInfo(Context context, AbilityInfo abilityInfo) {
        if (abilityInfo == null) {
            throw new NullPointerException("DebugDBInitProvider AbilityInfo cannot be null.");
        }
        context.getAbilityInfo().getURI();

        // So if the authorities equal the library internal ones, the developer forgot to set his applicationId
        if ("com.amitshekhar.debug.DebugDBInitProvider".equals(abilityInfo.getURI())) {
            throw new IllegalStateException("Incorrect ability authority in config. Most likely due to a "
                    + "missing applicationId variable in application\'s build.gradle.");
        }
    }

    @Override
    public ResultSet query(Uri uri, String[] columns, DataAbilityPredicates predicates) {
        return null;
    }

    @Override
    public int insert(Uri uri, ValuesBucket value) {

        return 999;
    }

    @Override
    public int delete(Uri uri, DataAbilityPredicates predicates) {
        return 0;
    }

    @Override
    public int update(Uri uri, ValuesBucket value, DataAbilityPredicates predicates) {
        return 0;
    }

    @Override
    public FileDescriptor openFile(Uri uri, String mode) {
        return null;
    }

    @Override
    public String[] getFileTypes(Uri uri, String mimeTypeFilter) {
        return new String[0];
    }

    @Override
    public PacMap call(String method, String arg, PacMap extras) {
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

}
