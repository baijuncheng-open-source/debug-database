package com.amitshekhar.debug.sqlite;

import com.amitshekhar.sqlite.DBFactory;
import com.amitshekhar.sqlite.SQLiteDB;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;

public class DebugDBFactory implements DBFactory {

    @Override
    public SQLiteDB create(Context context, String path, String password) {

        DatabaseHelper databaseHelper = new DatabaseHelper(context.getApplicationContext());
        StoreConfig storeConfig = StoreConfig.newDefaultConfig(path);

        return new DebugSQLiteDB(databaseHelper.getRdbStore(storeConfig, 1, new RdbOpenCallback() {
            @Override
            public void onCreate(RdbStore rdbStore) {

            }

            @Override
            public void onUpgrade(RdbStore rdbStore, int i, int i1) {

            }
        }));
    }

}
