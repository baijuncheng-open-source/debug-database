package com.amitshekhar.debug.encrypt.sqlite;

import com.amitshekhar.sqlite.SQLiteDB;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.data.rdb.StoreConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.*;

@PrepareForTest({DebugDBEncryptFactory.class,StoreConfig.class,DatabaseHelper.class, TextTool.class,Preferences.class})
@RunWith(PowerMockRunner.class)
public class DebugDBEncryptFactoryTest {
    @Before
    public void setUp() throws Exception {
        mockStatic(TextTool.class);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = NullPointerException.class)
    public void create() throws Exception {
        String path = "/aa.db";
        String password = "123";
        Context context = mock(Context.class);
        when(context.getApplicationContext()).thenReturn(context);
        DatabaseHelper databaseHelper = mock(DatabaseHelper.class);
        whenNew(DatabaseHelper.class).withArguments(context.getApplicationContext()).thenReturn(databaseHelper);
        StoreConfig.Builder storeConfigBuilder = mock(StoreConfig.Builder.class);
        whenNew(StoreConfig.Builder.class).withNoArguments().thenReturn(storeConfigBuilder);
        when(storeConfigBuilder.setName(path)).thenReturn(storeConfigBuilder);
        when(storeConfigBuilder.setEncryptKey(password.getBytes())).thenReturn(storeConfigBuilder);

        Preferences preferences = mock(Preferences.class);
        when(databaseHelper.getPreferences(anyString())).thenReturn(preferences);

        DebugDBEncryptFactory debugDBEncryptFactory = new DebugDBEncryptFactory();
        SQLiteDB sqLiteDB = debugDBEncryptFactory.create(context, path, password);
        assertNotNull(sqLiteDB);
    }
}