package com.amitshekhar.debug.encrypt.sqlite;

import ohos.agp.utils.TextTool;
import ohos.data.rdb.AbsRdbPredicates;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@PrepareForTest({AbsRdbPredicates.class,DebugEncryptSQLiteDB.class, TextTool.class})
@RunWith(PowerMockRunner.class)
public class DebugEncryptSQLiteDBTest {
    DebugEncryptSQLiteDB debugEncryptSQLiteDB;
    RdbStore database;
    @Before
    public void setUp() throws Exception {
        mockStatic(TextTool.class);
        database = mock(RdbStore.class);
        debugEncryptSQLiteDB = new DebugEncryptSQLiteDB(database);
        whenNew(DebugEncryptSQLiteDB.class).withAnyArguments().thenReturn(debugEncryptSQLiteDB);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void delete() throws Exception {
        String[] a = {"1=1"};
        RawRdbPredicates rdbPredicates = mock(RawRdbPredicates.class);
        whenNew(RawRdbPredicates.class).withAnyArguments().thenReturn(rdbPredicates);
        int users = debugEncryptSQLiteDB.delete("users", "1=1", a);
        assertNotNull(users);
    }

    @Test
    public void isOpen() {
        boolean open = debugEncryptSQLiteDB.isOpen();
        assertEquals(open,false);
    }

    @Test
    public void close() {
        debugEncryptSQLiteDB.close();
        verify(database,times(1)).close();
    }

    @Test
    public void rawQuery() {
        debugEncryptSQLiteDB.rawQuery("select * from users", null);
        verify(database,times(1)).querySql(anyString(),any());
    }

    @Test
    public void execSQL() {
        debugEncryptSQLiteDB.execSQL("select * from users");
        verify(database,times(1)).executeSql(anyString());
    }

    @Test
    public void insert() throws Exception {
        ValuesBucket valuesBucket = mock(ValuesBucket.class);
        whenNew(ValuesBucket.class).withAnyArguments().thenReturn(valuesBucket);
        valuesBucket.putLong("id", 1L);
        valuesBucket.putString("name", "222");
        Set<String> columnSet = new HashSet<>();
        columnSet.add("1");
        columnSet.add("2");
        when(valuesBucket.getColumnSet()).thenReturn(columnSet);
        long users = debugEncryptSQLiteDB.insert("users", "11", valuesBucket);
        assertNotNull(users);

    }

    @Test
    public void update() throws Exception {
        RawRdbPredicates rdbPredicates = mock(RawRdbPredicates.class);
        whenNew(RawRdbPredicates.class).withArguments("users").thenReturn(rdbPredicates);
        doNothing().when(rdbPredicates).setWhereClause("1=1");
        doNothing().when(rdbPredicates).setWhereArgs(new ArrayList<>());
        int users = debugEncryptSQLiteDB.delete("users", "1=1", new String[]{"11", "22"});
        assertNotNull(users);
    }

    @Test
    public void getVersion() {
        int version = debugEncryptSQLiteDB.getVersion();
        assertNotNull(version);
    }
}