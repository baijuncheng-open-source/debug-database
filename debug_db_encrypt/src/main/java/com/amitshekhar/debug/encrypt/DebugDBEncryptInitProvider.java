package com.amitshekhar.debug.encrypt;

import com.amitshekhar.DebugDB;
import com.amitshekhar.debug.encrypt.sqlite.DebugDBEncryptFactory;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.bundle.AbilityInfo;
import ohos.data.dataability.DataAbilityPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.PacMap;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;

public class DebugDBEncryptInitProvider extends Ability {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "DebugDBEncryptInitProvider");

    public DebugDBEncryptInitProvider() {
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        HiLog.info(LABEL_LOG,"DebugDBEncryptInitProvider-onStart");
        attachInfo(this,getAbilityInfo());
        DebugDB.initialize(getContext(), new DebugDBEncryptFactory());
    }

    @Override
    public ResultSet query(Uri uri, String[] columns, DataAbilityPredicates predicates) {
        return null;
    }

    @Override
    public int insert(Uri uri, ValuesBucket value) {
        return 0;
    }

    @Override
    public int delete(Uri uri, DataAbilityPredicates predicates) {
        return 0;
    }

    @Override
    public int update(Uri uri, ValuesBucket value, DataAbilityPredicates predicates) {
        return 0;
    }

    @Override
    public FileDescriptor openFile(Uri uri, String mode) {
        return null;
    }

    @Override
    public String[] getFileTypes(Uri uri, String mimeTypeFilter) {
        return new String[0];
    }

    @Override
    public PacMap call(String method, String arg, PacMap extras) {
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    public void attachInfo(Context context, AbilityInfo abilityInfo) {
        if (abilityInfo == null) {
            throw new NullPointerException("DebugDBEncryptInitProvider AbilityInfo cannot be null.");
        }
        context.getAbilityInfo().getURI();

        // So if the authorities equal the library internal ones, the developer forgot to set his applicationId
        if ("com.amitshekhar.debug.encrypt.DebugDBEncryptInitProvider".equals(abilityInfo.getURI())) {
            throw new IllegalStateException("Incorrect ability authority in config. Most likely due to a "
                    + "missing applicationId variable in application\'s build.gradle.");
        }
    }

}
