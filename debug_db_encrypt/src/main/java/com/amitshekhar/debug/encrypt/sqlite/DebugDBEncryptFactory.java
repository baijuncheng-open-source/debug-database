package com.amitshekhar.debug.encrypt.sqlite;

import com.amitshekhar.sqlite.DBFactory;
import com.amitshekhar.sqlite.SQLiteDB;
import com.amitshekhar.utils.DatabaseFileProvider;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;

import java.io.File;
import java.text.MessageFormat;
import java.util.logging.Logger;

public class DebugDBEncryptFactory implements DBFactory {

    private static final String DB_PASSWORD_RESOURCE = "DB_PASSWORD_{0}";
    private static final String TAG = "DebugDBEncryptFactory";

    @Override
    public SQLiteDB create(Context context, String path, String password) {
        Logger.getLogger(TAG).info("path:" + path + ", pwd:" + password);
        DatabaseHelper databaseHelper = new DatabaseHelper(context.getApplicationContext());
        StoreConfig.Builder storeConfigBuilder = new StoreConfig.Builder()
                .setName(path);
        if (!TextTool.isNullOrEmpty(password)) {
            storeConfigBuilder.setEncryptKey(password.getBytes());
        }
        Preferences preferences = databaseHelper.getPreferences(DatabaseFileProvider.NAME_PREFS);
        String nameWithoutExt = path;
        int i = path.lastIndexOf(File.separator);
        if (i >= 0)
            nameWithoutExt = path.substring(i);
        if (nameWithoutExt.endsWith(".db")) {
            nameWithoutExt = nameWithoutExt.substring(0, nameWithoutExt.lastIndexOf('.'));
        }
        String resourceName = MessageFormat.format(DB_PASSWORD_RESOURCE, nameWithoutExt.toUpperCase());
        preferences.putString(resourceName, password)
                .flushSync();
        RdbStore rdbStore = databaseHelper.getRdbStore(storeConfigBuilder.build(), 1, new RdbOpenCallback() {
            @Override
            public void onCreate(RdbStore rdbStore) {
            }

            @Override
            public void onUpgrade(RdbStore rdbStore, int i, int i1) {
            }
        });
        return new DebugEncryptSQLiteDB(rdbStore);
    }

}
