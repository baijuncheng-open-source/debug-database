/*
 *
 *  *    Copyright (C) 2019 Amit Shekhar
 *  *    Copyright (C) 2011 ohos Open Source Project
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.sample.database;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;
import ohos.data.rdb.ValuesBucket;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.resultset.ResultSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.logging.Logger;

/**
 * Created by amitshekhar on 16/11/16.
 */
public class ContactDBHelper extends RdbOpenCallback {

    public static final String DATABASE_NAME = "Contact.db";
    public static final String CONTACTS_TABLE_NAME = "contacts";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_NAME = "name";
    public static final String CONTACTS_COLUMN_EMAIL = "email";
    public static final String CONTACTS_COLUMN_STREET = "street";
    public static final String CONTACTS_COLUMN_CITY = "place";
    public static final String CONTACTS_COLUMN_PHONE = "phone";
    public static final String CONTACTS_CREATED_AT = "createdAt";
    private static final String TAG = "ContactDBHelper";
    private DatabaseHelper databaseHelper;
    private StoreConfig storeConfig;
    private RdbStore db;

    @Override
    public void onCreate(RdbStore db) {
        Logger.getLogger("TAG").info("Contact.db oncreate");
        db.executeSql(
                "create table contacts " +
                        "(id integer primary key, name text, phone text, email text, street text, place text, createdAt integer)"
        );
        String path = db.getPath();
        Logger.getLogger("TAG").info(path);
    }

    @Override
    public void onUpgrade(RdbStore db, int i, int i1) {
        db.executeSql("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    public ContactDBHelper(Context context) {
        databaseHelper = new DatabaseHelper(context.getApplicationContext());
        storeConfig = StoreConfig.newDefaultConfig(DATABASE_NAME);
        getDb();
    }

    private RdbStore getDb() {
        if (db!=null && db.isOpen())
            return db;
        return db = databaseHelper.getRdbStore(storeConfig, 1, this);
    }

    public boolean insertContact(String name, String phone, String email, String street, String place) {
        db = getDb();
        ValuesBucket contentValues = new ValuesBucket();
        contentValues.putString("name", name);
        contentValues.putString("phone", phone);
        contentValues.putString("email", email);
        contentValues.putString("street", street);
        contentValues.putString("place", place);
        contentValues.putLong(CONTACTS_CREATED_AT, Calendar.getInstance().getTimeInMillis());
        long l = db.insert("contacts", contentValues);
        Logger.getLogger(TAG).info("id - "+l);
        db.close();
        return l > 0;
    }

    public ResultSet getData(int id) {
        db = getDb();
        ResultSet res = db.querySql("select * from contacts where id=" + id + "", null);
        return res;
    }

    public int numberOfRows() {
        db = getDb();
        int numRows = (int) db.count(CONTACTS_TABLE_NAME, null, null);
        db.close();
        return numRows;
    }

    public boolean updateContact(Integer id, String name, String phone, String email, String street, String place) {
        db = getDb();
        ValuesBucket contentValues = new ValuesBucket();
        contentValues.putString("name", name);
        contentValues.putString("phone", phone);
        contentValues.putString("email", email);
        contentValues.putString("street", street);
        contentValues.putString("place", place);

        RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(CONTACTS_TABLE_NAME);
        rawRdbPredicates.setWhereClause("id = ? ");
        rawRdbPredicates.setWhereArgs(Arrays.asList(Integer.toString(id)));
        int updateCount = db.update(contentValues, rawRdbPredicates);
        db.close();
        return updateCount > 0;
    }

    public Integer deleteContact(Integer id) {
        db = getDb();
        RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(CONTACTS_TABLE_NAME);
        rawRdbPredicates.setWhereClause("id = ? ");
        rawRdbPredicates.setWhereArgs(Arrays.asList(Integer.toString(id)));
        int i = db.delete(rawRdbPredicates);
        db.close();
        return i;
    }

    public ArrayList<String> getAllCotacts() {
        ArrayList<String> arrayList = new ArrayList<>();

        db = getDb();
        ResultSet res = db.querySql("select * from contacts", null);
        res.goToFirstRow();

        while (!res.isEnded()) {
            arrayList.add(res.getString(res.getColumnIndexForName(CONTACTS_COLUMN_NAME)));
            res.goToNextRow();
        }
        res.close();
        db.close();
        return arrayList;
    }

    public int count() {
        db = getDb();
        ResultSet cursor = db.querySql("select COUNT(*) from contacts", null);
        try {
            if (cursor != null && cursor.getRowCount() > 0) {
                cursor.goToFirstRow();
                return cursor.getInt(0);
            } else {
                return 0;
            }
        } finally {
            cursor.close();
            db.close();
        }
    }
}
