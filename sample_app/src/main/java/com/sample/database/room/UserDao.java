package com.sample.database.room;

import java.util.List;

/**
 * Created by anandgaurav on 12/02/18.
 */

public interface UserDao {

    List<User> loadAll();

    List<User> loadAllByIds(List<Integer> userIds);

    void insert(User user);

    void insertAll(List<User> users);

    void delete(User user);

}
