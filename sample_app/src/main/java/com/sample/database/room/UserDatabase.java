package com.sample.database.room;

import ohos.data.orm.OrmDatabase;
import ohos.data.orm.annotation.Database;

/**
 * Created by anandgaurav on 12/02/18.
 */
@Database(entities = {User.class}, version = 1)
public abstract class UserDatabase extends OrmDatabase {
}
