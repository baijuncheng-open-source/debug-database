package com.sample;

import com.sample.database.CarDBHelper;
import com.sample.database.ContactDBHelper;
import com.sample.database.ExtTestDBHelper;
import com.sample.database.room.User;
import com.sample.database.room.UserDBHelper;
import com.sample.slice.MainAbilitySlice;
import com.sample.utils.Utils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class MainAbility extends Ability {
    private static final String TAG = "MainAbility";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

        Set<String> stringSet = new HashSet<>();
        stringSet.add("SetOne");
        stringSet.add("SetTwo");
        stringSet.add("SetThree");

        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        Preferences sharedPreferences = databaseHelper.getPreferences(getAbilityPackage().getBundleName()+".xml");

        Preferences prefsOne = databaseHelper.getPreferences("countPrefOne.xml");
        Preferences prefsTwo = databaseHelper.getPreferences("countPrefTwo.xml");

        sharedPreferences.putString("testOne", "one").flushSync();
        sharedPreferences.putInt("testTwo", 2).flushSync();
        sharedPreferences.putLong("testThree", 100000L).flushSync();
        sharedPreferences.putFloat("testFour", 3.01F).flushSync();
        sharedPreferences.putBoolean("testFive", true).flushSync();
        sharedPreferences.putStringSet("testSix", stringSet).flushSync();

        prefsOne.putString("testOneNew", "one").flushSync();

        prefsTwo.putString("testTwoNew", "two").flushSync();

        ContactDBHelper contactDBHelper = new ContactDBHelper(getApplicationContext());
        if (contactDBHelper.count() == 0) {
            for (int i = 0; i < 100; i++) {
                String name = "name_" + i;
                String phone = "phone_" + i;
                String email = "email_" + i;
                String street = "street_" + i;
                String place = "place_" + i;
                contactDBHelper.insertContact(name, phone, email, street, place);
            }
        }

        CarDBHelper carDBHelper = new CarDBHelper(getApplicationContext());
        if (carDBHelper.count() == 0) {
            for (int i = 0; i < 50; i++) {
                String name = "name_" + i;
                String color = "RED";
                float mileage = i + 10.45f;
                carDBHelper.insertCar(name, color, mileage);
            }
        }

        ExtTestDBHelper extTestDBHelper = new ExtTestDBHelper(getApplicationContext());
        if (extTestDBHelper.count() == 0) {
            for (int i = 0; i < 20; i++) {
                String value = "value_" + i;
                extTestDBHelper.insertTest(value);
            }
        }

        // Room database
        UserDBHelper userDBHelper = new UserDBHelper(this);
        if (userDBHelper.countInMemory() == 0) {
            List<User> userList = new ArrayList<>();
            for (int i = 0; i < 20; i++) {
                User user = new User();
                user.id = (long) (i + 1);
                user.name = "in_memory_user_" + i;
                userList.add(user);
            }
            Logger.getLogger(TAG).info("insertUserInMemory size:"+userList.size());
            userDBHelper.insertUserInMemory(userList);
        }

        Utils.setCustomDatabaseFiles(getApplicationContext());
        Utils.setInMemoryRoomDatabases(userDBHelper.getInMemoryDatabase().getRealDb());
    }
}
