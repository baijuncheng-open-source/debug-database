package com.sample.slice;

import com.sample.ResourceTable;
import com.sample.utils.Utils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Text text = (Text) findComponentById(ResourceTable.Id_text_helloworld);
        text.setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    public void showDebugDbAddress(Component view) {
        Utils.showDebugDBAddressLogToast(getApplicationContext());
    }

    @Override
    public void onClick(Component component) {
        showDebugDbAddress(component);
    }
}
