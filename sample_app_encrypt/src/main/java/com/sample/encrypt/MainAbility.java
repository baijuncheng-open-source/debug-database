package com.sample.encrypt;

import com.sample.encrypt.database.CarDBHelper;
import com.sample.encrypt.database.ContactDBHelper;
import com.sample.encrypt.database.ExtTestDBHelper;
import com.sample.encrypt.database.PersonDBHelper;
import com.sample.encrypt.database.room.User;
import com.sample.encrypt.database.room.UserDBHelper;
import com.sample.encrypt.slice.MainAbilitySlice;
import com.sample.encrypt.utils.Utils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());

        Set<String> stringSet = new HashSet<>();
        stringSet.add("SetOne");
        stringSet.add("SetTwo");
        stringSet.add("SetThree");

        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        Preferences sharedPreferences = databaseHelper.getPreferences(getBundleName()+".xml");

        Preferences prefsOne = databaseHelper.getPreferences("countPrefOne.xml");
        Preferences prefsTwo = databaseHelper.getPreferences("countPrefTwo.xml");

        sharedPreferences.putString("testOne", "one").flushSync();
        sharedPreferences.putInt("testTwo", 2).flushSync();
        sharedPreferences.putLong("testThree", 100000L).flushSync();
        sharedPreferences.putFloat("testFour", 3.01F).flushSync();
        sharedPreferences.putBoolean("testFive", true).flushSync();
        sharedPreferences.putStringSet("testSix", stringSet).flushSync();

        prefsOne.putString("testOneNew", "one").flushSync();

        prefsTwo.putString("testTwoNew", "two").flushSync();

        ContactDBHelper contactDBHelper = new ContactDBHelper(getApplicationContext());
        if (contactDBHelper.count() == 0) {
            for (int i = 0; i < 100; i++) {
                String name = "name_" + i;
                String phone = "phone_" + i;
                String email = "email_" + i;
                String street = "street_" + i;
                String place = "place_" + i;
                contactDBHelper.insertContact(name, phone, email, street, place);
            }
        }

        CarDBHelper carDBHelper = new CarDBHelper(getApplicationContext());
        if (carDBHelper.count() == 0) {
            for (int i = 0; i < 50; i++) {
                String name = "name_" + i;
                String color = "RED";
                float mileage = i + 10.45f;
                carDBHelper.insertCar(name, color, mileage);
            }
        }

        ExtTestDBHelper extTestDBHelper = new ExtTestDBHelper(getApplicationContext());
        if (extTestDBHelper.count() == 0) {
            for (int i = 0; i < 20; i++) {
                String value = "value_" + i;
                extTestDBHelper.insertTest(value);
            }
        }

        // Create Person encrypted database
        PersonDBHelper personDBHelper = new PersonDBHelper(getApplicationContext());
        if (personDBHelper.count() == 0) {
            for (int i = 0; i < 100; i++) {
                String firstName = PersonDBHelper.PERSON_COLUMN_FIRST_NAME + "_" + i;
                String lastName = PersonDBHelper.PERSON_COLUMN_LAST_NAME + "_" + i;
                String address = PersonDBHelper.PERSON_COLUMN_ADDRESS + "_" + i;
                personDBHelper.insertPerson(firstName, lastName, address);
            }
        }

        // Room database
        UserDBHelper userDBHelper = new UserDBHelper(this);

        // Room inMemory database
        if (userDBHelper.countInMemory() == 0) {
            List<User> userList = new ArrayList<>();
            for (int i = 0; i < 20; i++) {
                User user = new User();
                user.id = (long) (i + 1);
                user.name = "in_memory_user_" + i;
                userList.add(user);
            }
            userDBHelper.insertUserInMemory(userList);
        }

        Utils.setCustomDatabaseFiles(getApplicationContext());
        Utils.setInMemoryRoomDatabases(userDBHelper.getInMemoryDatabase().getRealDb());
    }
}
