/*
 *
 *  *    Copyright (C) 2019 Amit Shekhar
 *  *    Copyright (C) 2011 ohos Open Source Project
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.sample.encrypt.database;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;
import ohos.data.rdb.ValuesBucket;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.resultset.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Created by amitshekhar on 06/02/17.
 */

public class CarDBHelper {

    public static final String DATABASE_NAME = "Car.db";
    public static final String CARS_TABLE_NAME = "cars";
    public static final String CARS_COLUMN_ID = "id";
    public static final String CARS_COLUMN_NAME = "name";
    public static final String CARS_COLUMN_COLOR = "color";
    public static final String CCARS_COLUMN_MILEAGE = "mileage";
    private DatabaseHelper databaseHelper;
    private StoreConfig storeConfig;
    private RdbStore db;
    private static RdbOpenCallback rdbOpenCallback = new RdbOpenCallback() {
        @Override
        public void onCreate(RdbStore db) {
            Logger.getLogger("TAG").info("Car.db oncreate");
            db.executeSql(
                    "create table cars " +
                            "(id integer primary key, name text, color text, mileage real)"
            );

            db.executeSql("create table [transaction] (id integer primary key, name text)");

            for (int i = 0; i < 10; i++) {
                db.executeSql("insert into [transaction] (name) values ('hello');");
            }
            String path = db.getPath();
            Logger.getLogger("TAG").info(path);
        }

        @Override
        public void onUpgrade(RdbStore db, int i, int i1) {
            db.executeSql("DROP TABLE IF EXISTS cars");
            onCreate(db);
        }
    };

    public CarDBHelper(Context context) {
        databaseHelper = new DatabaseHelper(context.getApplicationContext());
        storeConfig = StoreConfig.newDefaultConfig(DATABASE_NAME);
        getDb();
    }

    private RdbStore getDb() {
        if (db!=null&&db.isOpen())
            return db;
        return db = databaseHelper.getRdbStore(storeConfig, 1, rdbOpenCallback);
    }

    public boolean insertCar(String name, String color, float mileage) {
        db = getDb();
        ValuesBucket contentValues = new ValuesBucket();
        contentValues.putString("name", name);
        contentValues.putString("color", color);
        contentValues.putFloat("mileage", mileage);
        long l = db.insert("cars", contentValues);
        db.close();
        return l > 0;
    }

    public ResultSet getData(int id) {
        db = getDb();
        ResultSet rs = db.querySql("select * from cars where id=" + id + "",null);
        return rs;
    }

    public int numberOfRows() {
        db = getDb();
        int numRows = (int) db.count(CARS_TABLE_NAME, null, null);
        db.close();
        return numRows;
    }

    public boolean updateCar(Integer id, String name, String color, float mileage) {
        db = getDb();
        ValuesBucket contentValues = new ValuesBucket();
        contentValues.putString("name", name);
        contentValues.putString("color", color);
        contentValues.putFloat("mileage", mileage);

        RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(CARS_TABLE_NAME);
        rawRdbPredicates.setWhereClause("id = ? ");
        rawRdbPredicates.setWhereArgs(Arrays.asList(Integer.toString(id)));
        int updateCount = db.update(contentValues, rawRdbPredicates);
        db.close();
        return updateCount > 0;
    }

    public Integer deleteCar(Integer id) {
        db = getDb();
        RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(CARS_TABLE_NAME);
        rawRdbPredicates.setWhereClause("id = ? ");
        rawRdbPredicates.setWhereArgs(Arrays.asList(Integer.toString(id)));
        int i = db.delete(rawRdbPredicates);
        db.close();
        return i;
    }

    public ArrayList<String> getAllCars() {
        ArrayList<String> arrayList = new ArrayList<>();

        db = getDb();
        ResultSet res = db.querySql("select * from cars", null);
        res.goToFirstRow();

        while (!res.isEnded()) {
            arrayList.add(res.getString(res.getColumnIndexForName(CARS_COLUMN_NAME)));
            res.goToNextRow();
        }
        res.close();
        db.close();
        return arrayList;
    }

    public int count() {
        db = getDb();
        ResultSet cursor = db.querySql("select COUNT(*) from cars", null);
        try {
            if (cursor != null && cursor.getRowCount() > 0) {
                cursor.goToFirstRow();
                return cursor.getInt(0);
            } else {
                return 0;
            }
        } finally {
            cursor.close();
            db.close();
        }
    }
}
