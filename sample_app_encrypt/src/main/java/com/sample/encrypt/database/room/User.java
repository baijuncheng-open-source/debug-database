package com.sample.encrypt.database.room;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

/**
 * Created by anandgaurav on 12/02/18.
 */
@Entity(tableName = "users")
public class User extends OrmObject {

    @PrimaryKey
    public Long id;

    public String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
