package com.sample.encrypt.database.room;

import com.amitshekhar.sqlite.InMemoryDebugSQLiteDB;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;

import java.util.List;

/**
 * Created by anandgaurav on 12/02/18.
 */

public class UserDBHelper {

    private static final String TAG = "UserDBHelper";
    private static final String USER_DB_ALIAS = "User";
    private static final String USER_DB = "User.db";
    private static final int USER_DB_VERSION = 1;
    private final OrmContext appDatabase;
    private final UserDaoImpl inMemoryAppDatabase;

    public UserDBHelper(Context context) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        appDatabase = databaseHelper.getOrmContext(USER_DB_ALIAS, USER_DB, UserDatabase.class);

        StoreConfig storeConfig = StoreConfig.newDefaultConfig(USER_DB);
        RdbStore rdbStore = databaseHelper.getRdbStore(storeConfig, USER_DB_VERSION, new RdbOpenCallback() {
            @Override
            public void onCreate(RdbStore db) {

            }

            @Override
            public void onUpgrade(RdbStore db, int i, int i1) {
            }
        });
        InMemoryDebugSQLiteDB inMemoryDebugSQLiteDB = new InMemoryDebugSQLiteDB(rdbStore);
        inMemoryAppDatabase = new UserDaoImpl(inMemoryDebugSQLiteDB);
    }

    public void insertUserInMemory(List<User> userList) {
        inMemoryAppDatabase.insertAll(userList);
    }

    public int countInMemory() {
        return inMemoryAppDatabase.loadAll().size();
    }

    public InMemoryDebugSQLiteDB getInMemoryDatabase() {
        return inMemoryAppDatabase.getInMemoryDatabase();
    }
}
