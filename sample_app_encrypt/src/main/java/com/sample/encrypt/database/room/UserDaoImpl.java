/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sample.encrypt.database.room;

import com.amitshekhar.sqlite.InMemoryDebugSQLiteDB;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;

import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {
    private InMemoryDebugSQLiteDB mInMemoryDebugSQLiteDB;

    public UserDaoImpl(InMemoryDebugSQLiteDB inMemoryDebugSQLiteDB) {
        mInMemoryDebugSQLiteDB = inMemoryDebugSQLiteDB;
    }

    public InMemoryDebugSQLiteDB getInMemoryDatabase() {
        return mInMemoryDebugSQLiteDB;
    }

    @Override
    public List<User> loadAll() {
        List<User> list = new ArrayList<>();
        ResultSet resultSet = mInMemoryDebugSQLiteDB.rawQuery("select * from users", null);
        if (resultSet.goToFirstRow()) {
            User user;
            int index;
            do {
                user = new User();
                index = resultSet.getColumnIndexForName("id");
                user.id = resultSet.getLong(index);
                user.name = resultSet.getString(index);
                list.add(user);
            } while (resultSet.goToNextRow());
        }
        resultSet.close();
        return list;
    }

    @Override
    public List<User> loadAllByIds(List<Integer> userIds) {
        List<User> list = new ArrayList<>();
        if (userIds == null || userIds.isEmpty())
            return list;
        StringBuilder stringBuilder = new StringBuilder(userIds.get(0));
        for (int i = 1; i < userIds.size(); i++) {
            stringBuilder.append(",").append(userIds.get(i));
        }
        ResultSet resultSet = mInMemoryDebugSQLiteDB.rawQuery("SELECT * FROM users WHERE id IN (?)", new String[]{stringBuilder.toString()});
        if (resultSet.goToFirstRow()) {
            User user;
            int index;
            do {
                user = new User();
                index = resultSet.getColumnIndexForName("id");
                user.id = resultSet.getLong(index);
                user.name = resultSet.getString(index);
                list.add(user);
            } while (resultSet.goToNextRow());
        }
        resultSet.close();
        return list;
    }

    @Override
    public void insert(User user) {
        if (user == null)
            return;
        ValuesBucket valuesBucket = new ValuesBucket();
        valuesBucket.putLong("id", user.id);
        valuesBucket.putString("name", user.name);
        mInMemoryDebugSQLiteDB.insert("users", null, valuesBucket);
    }

    @Override
    public void insertAll(List<User> users) {
        if (users == null || users.isEmpty())
            return;
        mInMemoryDebugSQLiteDB.beginTransaction();
        for (User user : users) {
            mInMemoryDebugSQLiteDB.execSQL("insert into users(id,name) values(" + user.id + ",'" + user.name + "')");
        }
        mInMemoryDebugSQLiteDB.endTransaction();
    }

    @Override
    public void delete(User user) {
        if (user == null || user.id == null)
            return;
        mInMemoryDebugSQLiteDB.delete("users", "id=?", new String[]{String.valueOf(user.id)});
    }
}
