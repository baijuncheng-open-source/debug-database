package com.sample.encrypt.database;

import ohos.app.AbilityContext;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;
import ohos.data.rdb.ValuesBucket;
import ohos.data.resultset.ResultSet;
import ohos.data.resultset.ResultSetHook;

import java.io.File;
import java.util.Calendar;
import java.util.logging.Logger;

public class ExtTestDBHelper {

    public static final String DIR_NAME = "custom_dir";
    public static final String DATABASE_NAME = "ExtTest.db";
    public static final String TEST_TABLE_NAME = "test";
    public static final String TEST_ID = "id";
    public static final String TEST_COLUMN_VALUE = "value";
    public static final String TEST_CREATED_AT = "createdAt";
    private DatabaseHelper databaseHelper;
    private StoreConfig storeConfig;
    private RdbStore db;
    private static RdbOpenCallback rdbOpenCallback = new RdbOpenCallback() {
        @Override
        public void onCreate(RdbStore db) {
            Logger.getLogger("TAG").info("ExtTest.db oncreate");
            db.executeSql(
                    String.format(
                            "create table %s (%s integer primary key, %s text, %s integer)",
                            TEST_TABLE_NAME,
                            TEST_ID,
                            TEST_COLUMN_VALUE,
                            TEST_CREATED_AT
                    )
            );
            String path = db.getPath();
            Logger.getLogger("TAG").info(path);
        }

        @Override
        public void onUpgrade(RdbStore db, int i, int i1) {
            db.executeSql("DROP TABLE IF EXISTS " + TEST_TABLE_NAME);
            onCreate(db);
        }
    };

    private static ResultSetHook resultSetHook = new ResultSetHook() {
        @Override
        public void createHook(String s, String[] strings, ResultSet resultSet) {

        }
    };

    public ExtTestDBHelper(Context context) {
        databaseHelper = new DatabaseHelper(context.getApplicationContext());
        storeConfig = new StoreConfig.Builder()
                .setName(DATABASE_NAME)
                .build();
        getDb();
    }

    private RdbStore getDb() {
        if (db!=null&&db.isOpen())
            return db;
        return databaseHelper.getRdbStore(storeConfig, 1, rdbOpenCallback);
    }

    public boolean insertTest(String value) {
        db = getDb();
        ValuesBucket contentValues = new ValuesBucket();
        contentValues.putString("value", value);
        contentValues.putLong(TEST_CREATED_AT, Calendar.getInstance().getTimeInMillis());
        long l = db.insert(TEST_TABLE_NAME, contentValues);
        db.close();
        return l > 0;
    }

    public int count() {
        db = getDb();
        ResultSet cursor = db.querySql("select COUNT(*) from " + TEST_TABLE_NAME, null);
        try {
            if (cursor != null && cursor.getRowCount() > 0) {
                cursor.goToFirstRow();
                return cursor.getInt(0);
            } else {
                return 0;
            }
        } finally {
            cursor.close();
            db.close();
        }
    }

    private static class CustomDatabasePathContext extends AbilityContext {

        public CustomDatabasePathContext(Context base) {
            super(base);
        }

        public File getDatabasePath(String name) {
            File databaseDir = new File(String.format("%s/%s", getFilesDir(), ExtTestDBHelper.DIR_NAME));
            databaseDir.mkdirs();
            File databaseFile = new File(String.format("%s/%s/%s", getFilesDir(), ExtTestDBHelper.DIR_NAME, name));
            return databaseFile;
        }
    }
}
