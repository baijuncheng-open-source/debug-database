/*
 *
 *  *    Copyright (C) 2019 Amit Shekhar
 *  *    Copyright (C) 2011 ohos Open Source Project
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package com.sample.encrypt.database;

import com.amitshekhar.utils.DatabaseFileProvider;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;
import ohos.data.rdb.ValuesBucket;
import ohos.data.rdb.RawRdbPredicates;
import ohos.data.resultset.ResultSet;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

public class PersonDBHelper {

    public static final String DATABASE_NAME = "Person.db";
    public static final String PERSON_TABLE_NAME = "person";
    public static final String PERSON_COLUMN_ID = "id";
    public static final String PERSON_COLUMN_FIRST_NAME = "first_name";
    public static final String PERSON_COLUMN_LAST_NAME = "last_name";
    public static final String PERSON_COLUMN_ADDRESS = "address";
    private static final String DB_PASSWORD = "a_password";
    private DatabaseHelper databaseHelper;
    private StoreConfig storeConfig;
    private RdbStore db;
    private static RdbOpenCallback rdbOpenCallback = new RdbOpenCallback() {
        @Override
        public void onCreate(RdbStore db) {
            Logger.getLogger("TAG").info("Person.db oncreate");
            db.executeSql(
                    "create table person " +
                            "(id integer primary key, first_name text, last_name text, address text)"
            );
            String path = db.getPath();
            Logger.getLogger("TAG").info(path);
        }

        @Override
        public void onUpgrade(RdbStore db, int i, int i1) {
            db.executeSql("DROP TABLE IF EXISTS person");
            onCreate(db);
        }
    };

    public PersonDBHelper(Context context) {
        databaseHelper = new DatabaseHelper(context.getApplicationContext());
        storeConfig = new StoreConfig.Builder()
                .setName(DATABASE_NAME)
                .setEncryptKey(DB_PASSWORD.getBytes())
                .build();
        Preferences preferences = databaseHelper.getPreferences(DatabaseFileProvider.NAME_PREFS);
        String nameWithoutExt = DATABASE_NAME;
        if (nameWithoutExt.endsWith(".db")) {
            nameWithoutExt = nameWithoutExt.substring(0, nameWithoutExt.lastIndexOf('.'));
        }
        String resourceName = MessageFormat.format(nameWithoutExt, nameWithoutExt.toUpperCase());
        preferences.putString(resourceName,DB_PASSWORD)
                .flushSync();
        getDb();
    }

    private RdbStore getDb() {
        if (db!=null&&db.isOpen())
            return db;
        return databaseHelper.getRdbStore(storeConfig, 1, rdbOpenCallback);
    }

    public boolean insertPerson(String firstName, String lastName, String address) {
        db = getDb();
        ValuesBucket contentValues = new ValuesBucket();
        contentValues.putString("first_name", firstName);
        contentValues.putString("last_name", lastName);
        contentValues.putString("address", address);
        long l = db.insert("person", contentValues);
        db.close();
        return l > 0;
    }

    public ResultSet getData(int id) {
        db = getDb();
        ResultSet res = db.querySql("select * from person where id=" + id + "", null);
        return res;
    }

    public int numberOfRows() {
        db = getDb();
        int numRows = (int) db.count(PERSON_TABLE_NAME, null, null);
        db.close();
        return numRows;
    }

    public boolean updatePerson(Integer id, String firstName, String lastName, String address, float mileage) {
        db = getDb();
        ValuesBucket contentValues = new ValuesBucket();
        contentValues.putString("first_name", firstName);
        contentValues.putString("last_name", lastName);
        contentValues.putString("address", address);

        RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(PERSON_TABLE_NAME);
        rawRdbPredicates.setWhereClause("id = ? ");
        rawRdbPredicates.setWhereArgs(Arrays.asList(Integer.toString(id)));
        int updateCount = db.update(contentValues, rawRdbPredicates);
        db.close();
        return updateCount > 0;
    }

    public Integer deletePerson(Integer id) {
        db = getDb();
        RawRdbPredicates rawRdbPredicates = new RawRdbPredicates(PERSON_TABLE_NAME);
        rawRdbPredicates.setWhereClause("id = ? ");
        rawRdbPredicates.setWhereArgs(Arrays.asList(Integer.toString(id)));
        int i = db.delete(rawRdbPredicates);
        db.close();
        return i;
    }

    public ArrayList<String> getAllPerson() {
        ArrayList<String> arrayList = new ArrayList<>();

        db = getDb();
        ResultSet res = db.querySql("select * from person", null);
        res.goToFirstRow();

        while (!res.isEnded()) {
            arrayList.add(
                    res.getString(res.getColumnIndexForName(PERSON_COLUMN_FIRST_NAME)) + " " +
                            res.getString(res.getColumnIndexForName(PERSON_COLUMN_LAST_NAME)));
            res.goToNextRow();
        }
        res.close();
        db.close();
        return arrayList;
    }

    public int count() {
        db = getDb();
        ResultSet cursor = db.querySql("select COUNT(*) from person", null);
        try {
            if (cursor != null && cursor.getRowCount() > 0) {
                cursor.goToFirstRow();
                return cursor.getInt(0);
            } else {
                return 0;
            }
        } finally {
            cursor.close();
            db.close();
        }
    }
}
